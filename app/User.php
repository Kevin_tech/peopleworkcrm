<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "usuario", "password",
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        "password", "remember_token",
    ];

    protected $table = "usuario";
    protected $dates = ["deleted_at"];

    public function documentoasignado()
    {
        return $this->hasMany("App\DocumentoAsignacion", "id_usuario_asignado");
    }

    public function documentoasigno()
    {
        return $this->hasMany("App\DocumentoAsignacion", "id_usuario_asigno");
    }

    public function documentocreado()
    {
        return $this->hasMany("App\Documento", "id_usuario_creo");
    }

    public function modulos()
    {
        return $this->hasMany("App\UsuarioModulo", "id_usuario");
    }

    public function oficioasignado()
    {
        return $this->hasMany("App\OficioAsignacion", "id_usuario_asignado");
    }

    public function oficiocreado()
    {
        return $this->hasMany("App\Oficio", "id_usuario_creo");
    }

    public function oficiovobo()
    {
        return $this->hasMany("App\Oficio", "id_usuario_vobo");
    }

    public function profesion()
    {
        return $this->belongsTo("App\Profesion", "id_profesion");
    }

    public function puesto()
    {
        return $this->belongsTo("App\Puesto", "id_puesto");
    }

    public function unidad()
    {
        return $this->belongsTo("App\Unidad", "id_unidad");
    }
}
