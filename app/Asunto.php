<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asunto extends Model
{
    //
    use SoftDeletes;

    protected $table = 'asunto';
    protected $dates = ['deleted_at'];

    public function unidad()
    {
        return $this->belongsTo("App\Unidad", "id_unidad");
    }

    public function oficio()
    {
        return $this->hasMany("App\Oficio", "id_asunto");
    }
}
