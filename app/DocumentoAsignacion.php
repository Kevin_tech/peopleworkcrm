<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentoAsignacion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'documentoasignacion';
    protected $dates = ['deleted_at'];

    public function documento()
    {
        return $this->belongsTo("App\Documento", "id_documento");
    }

    public function estado()
    {
        return $this->belongsTo("App\Estado", "id_estado");
    }

    public function usuario_asignado()
    {
        return $this->belongsTo("App\User", "id_usuario_asignado");
    }

    public function usuario_asigno()
    {
        return $this->belongsTo("App\User", "id_usuario_asigno");
    }
}
