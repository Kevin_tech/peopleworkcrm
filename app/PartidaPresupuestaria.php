<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartidaPresupuestaria extends Model
{
    //
    use SoftDeletes;

    protected $table = 'rrhh_partidapresupuestaria';
    protected $dates = ['deleted_at'];

    public function fuente_financiamiento()
    {
        return $this->belongsTo("App\FuenteFinanciamiento", "id_fuente_financiamiento");
    }

    public function renglon()
    {
        return $this->belongsTo("App\Renglon", "id_renglon");
    }
}
