<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get("/", function () {
    return view("login");
});

Route::get("/app", function () {
    return view("index");
});

Route::get("/login", function () {
    return view("login");
});

Route::get("/prueba", function () {
	$data = array();
    $pdf = PDF::loadView("pdf", $data);
	return $pdf->download("invoice.pdf");
});

Route::post("/prueba/registro", "AuthenticateController@register");


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(["middleware" => ["web"]], function () {
    //
});

Route::group(["prefix" => "api"], function()
{
	/*
	Route::resource("authenticate", "AuthenticateController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	*/
	// Authentication
	Route::get("authenticate", "AuthenticateController@getAuthenticatedUser");
	
	Route::post("authenticate/login", "AuthenticateController@authenticate");

	// Asunto
	Route::resource("asunto", "AsuntoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Boleta Autorizacion
	Route::resource("boletaautorizacion", "BoletaAutorizacionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Contrato
	Route::resource("contrato", "ContratoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Departamento
	Route::resource("departamento", "DepartamentoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Documentos
	Route::resource("documento", "DocumentoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	Route::get("documento/{documento}/imagen", "DocumentoController@showImage");
	
	Route::resource("documento/anulacion", "DocumentoAnulacionController",
					["only" => ["destroy", "store", "update"]]);
	
	Route::resource("documento/asignacion", "DocumentoAsignacionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	Route::resource("documento/observacion", "DocumentoObservacionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Estado
	Route::resource("estado", "EstadoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Estado Civil
	Route::resource("estadocivil", "EstadoCivilController",
					["only" => ["index"]]);

	// Fuente de Financiamiento
	Route::resource("fuentefinanciamiento", "FuenteFinanciamientoController",
					["only" => ["index"]]);
	
	// Institucion
	Route::resource("institucion", "InstitucionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Municipio
	Route::resource("municipio", "MunicipioController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Nivel Academico
	Route::resource("nivelacademico", "NivelAcademicoController",
					["only" => ["index"]]);

	// Notificacion
	Route::resource("notificacion", "NotificacionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	// Oficio
	Route::resource("oficio", "OficioController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	Route::get("oficio/{oficio}/imagen", "OficioController@showImage");
	
	Route::resource("oficio/asignacion", "OficioAsignacionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Pago Cheque
	Route::resource("pagocheque", "PagoChequeController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Pais
	Route::resource("pais", "PaisController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Partida Presupuestaria
	Route::resource("partidapresupuestaria", "PartidaPresupuestariaController",
					["only" => ["index"]]);

	// Password User
	Route::post("password", "AuthenticateController@updateUserPassword");
	
	Route::get("password/{usuario}", "AuthenticateController@resetUserPassword"); // admin
	
	Route::put("password/{usuario}", "AuthenticateController@updateUserPasswordAdmin");

	// Procedimientos
	Route::resource("procedimiento", "ProcedimientoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	Route::resource("procedimiento/actividad", "ProcedimientoActividadController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	// Profesion
	Route::resource("profesion", "ProfesionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	// Puesto
	Route::resource("puesto", "PuestoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Renglon
	Route::resource("renglon", "RenglonController",
					["only" => ["index"]]);

	// Sequence
	Route::resource("sequence", "SequenceController",
					["only" => ["destroy", "store"]]);
	
	Route::post("sequence/sub", "SequenceController@subsequence");

	// Temporal de Personas
	Route::resource("temppersona", "TempPersonaController",
					["only" => ["index"]]);

	// Tipo Contrato
	Route::resource("tipocontrato", "TipoContratoController",
					["only" => ["index"]]);

	// Tipo Servicio
	Route::resource("tiposervicio", "TipoServicioController",
					["only" => ["index"]]);

	// Unidad
	Route::resource("unidad", "UnidadController",
					["only" => ["destroy", "index", "show", "store", "update"]]);

	// Usuario
	Route::resource("usuario", "AuthenticateController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	Route::put("usuario/{usuario}/status", "AuthenticateController@activateUser");
	
	Route::delete("usuario/{usuario}/status", "AuthenticateController@deactivateUser");
});

Route::group(["prefix" => "article"], function()
{
	Route::get("boletaautorizacion", function () {
		return view("article.boletaautorizacion");
	});

	Route::get("contrato", function () {
		return view("article.contrato");
	});

	Route::get("documento", function () {
		return view("article.documento");
	});

	Route::get("fuentefinanciamiento", function () {
		return view("article.fuentefinanciamiento");
	});

	Route::get("oficio", function () {
		return view("article.oficio");
	});

	Route::get("pagocheque", function () {
		return view("article.pagocheque");
	});

	Route::get("partidapresupuestaria", function () {
		return view("article.partidapresupuestaria");
	});

	Route::get("procedimiento", function () {
		return view("article.procedimiento");
	});

	Route::get("usuario", function () {
		return view("article.usuario");
	});
});

Route::group(["prefix" => "form"], function()
{
	Route::get("boletaautorizacion", function () {
		return view("form.boletaautorizacion");
	});

	Route::get("contrato", function () {
		return view("form.contrato");
	});

	Route::get("documento", function () {
		return view("form.documento");
	});

	Route::get("fuentefinanciamiento", function () {
		return view("form.fuentefinanciamiento");
	});

	Route::get("oficio", function () {
		return view("form.oficio");
	});

	Route::get("pagocheque", function () {
		return view("form.pagocheque");
	});

	Route::get("partidapresupuestaria", function () {
		return view("form.partidapresupuestaria");
	});

	Route::get("procedimiento", function () {
		return view("form.procedimiento");
	});

	Route::get("puesto", function () {
		return view("form.puesto");
	});

	Route::get("recursoshumanos", function () {
		return view("form.recursoshumanos");
	});

	Route::get("report.documento", function () {
		return view("form.report_documento");
	});

	Route::get("report.expedientes", function () {
		return view("form.report_expedientes");
	});

	Route::get("report.firmaelectronica", function () {
		return view("form.report_firmaelectronica");
	});

	Route::get("report.operaciones", function () {
		return view("form.report_operaciones");
	});

	Route::get("solicitudanulacion", function () {
		return view("form.solicitudanulacion");
	});

	Route::get("unidad", function () {
		return view("form.unidad");
	});

	Route::get("usuario", function () {
		return view("form.usuario");
	});
});

Route::group(["prefix" => "list"], function()
{
	Route::get("boletaautorizacion", function () {
		return view("list.boletaautorizacion");
	});

	Route::get("contrato", function () {
		return view("list.contrato");
	});

	Route::get("documento", function () {
		return view("list.documento");
	});

	Route::get("fuentefinanciamiento", function () {
		return view("list.fuentefinanciamiento");
	});

	Route::get("oficio", function () {
		return view("list.oficio");
	});

	Route::get("pagocheque", function () {
		return view("list.pagocheque");
	});

	Route::get("partidapresupuestaria", function () {
		return view("list.partidapresupuestaria");
	});

	Route::get("procedimiento", function () {
		return view("list.procedimiento");
	});

	Route::get("puesto", function () {
		return view("list.puesto");
	});

	Route::get("unidad", function () {
		return view("list.unidad");
	});

	Route::get("usuario", function () {
		return view("list.usuario");
	});
});

Route::group(["prefix" => "module"], function()
{
	Route::get("administracion", function () {
		return view("module.administracion");
	});

	Route::get("boletasautorizacion", function () {
		return view("module.boletasautorizacion");
	});

	Route::get("contratos", function () {
		return view("module.contratos");
	});

	Route::get("dashboard", function () {
		return view("module.dashboard");
	});

	Route::get("documentos", function () {
		return view("module.documentos");
	});

	Route::get("fuentesfinanciamiento", function () {
		return view("module.fuentesfinanciamiento");
	});

	Route::get("header", function () {
		return view("module.header");
	});

	Route::get("logout", function () {
		return view("module.logout");
	});

	Route::get("menurecursoshumanos", function () {
		return view("module.menurecursoshumanos");
	});

	Route::get("oficios", function () {
		return view("module.oficios");
	});

	Route::get("pagos", function () {
		return view("module.pagos");
	});

	Route::get("partidaspresupuestarias", function () {
		return view("module.partidaspresupuestarias");
	});

	Route::get("procedimientos", function () {
		return view("module.procedimientos");
	});

	Route::get("recursoshumanos", function () {
		return view("module.recursoshumanos");
	});

	Route::get("reportes", function () {
		return view("module.reportes");
	});

	Route::get("usuarios", function () {
		return view("module.usuarios");
	});
});

Route::group(["prefix" => "report"], function()
{
	Route::get("/boletaautorizacion", "ReporteController@reporteBoletaAutorizacion");

	Route::get("/documento", "ReporteController@reporteDocumento");

	Route::get("/expedientes", "ReporteController@reporteExpedientes");

	Route::get("/firmaelectronica", "ReporteController@reporteFirmaElectronica");

	Route::get("/operaciones", "ReporteController@reporteOperaciones");

	Route::get("/procedimiento", "ReporteController@reporteProcedimiento");

	Route::get("/pagocheque", "ReporteController@reportePagoCheque");
});