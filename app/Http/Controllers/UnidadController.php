<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Unidad;

class UnidadController extends Controller
{
	public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth");
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = Unidad::with("asunto", "usuario")->find( $id );

    	if( $record )
    	{
            if( count( $record->asunto ) > 0 || count( $record->usuario ) > 0 )
            {
                $response = response()->json([
                    "msg"       => "Error, foreign key",
                    "id"        => $id
                ], 409);
            }
            else
            {
        		$record->deleted_at	= time();
        		$record->save();

        		if( $record->trashed() )
        		{
        			$response = response()->json([
    					"msg"		=> "Record deleted",
    					"id"		=> $id
    				], 200);
        		}
        		else
        		{
        			$response = response()->json([
    					"msg"		=> "Error",
    					"id"		=> $id
    				], 400);
        		}
            }
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	$records = Unidad::with("director", "gerente", "supervisor")->orderBy("nombre", "asc")->get();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = Unidad::with("director", "gerente", "supervisor")->find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_director" => "required|integer",
			"id_gerente" => "required|integer",
			"id_supervisor" => "required|integer",
			"nombre" => "required",
			"siglas" => "alpha_num|max:10",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new Unidad();

    	$record->id_director 	= $request->id_director;
    	$record->id_gerente 	= $request->id_gerente;
    	$record->id_supervisor 	= $request->id_supervisor;
    	$record->nombre 		= $request->nombre;
    	$record->siglas 		= $request->siglas;

    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_director" => "required|integer|exists:usuario,id",
			"id_gerente" => "required|integer|exists:usuario,id",
			"id_supervisor" => "required|integer|exists:usuario,id",
			"nombre" => "required",
			"siglas" => "alpha_num|max:10",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = Unidad::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->id_director 	= $request->id_director;
	    	$record->id_gerente 	= $request->id_gerente;
	    	$record->id_supervisor 	= $request->id_supervisor;
	    	$record->nombre 		= $request->nombre;
	    	$record->siglas 		= $request->siglas;

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
