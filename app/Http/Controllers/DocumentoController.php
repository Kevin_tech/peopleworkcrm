<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use LRedis;

use App\Documento;
use App\DocumentoAsignacion;
use App\Notificacion;

class DocumentoController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth");
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = Documento::with("asignacion")->find( $id );

    	if( $record )
    	{
            if( count( $record->asignacion ) > 0 )
            {
                $response = response()->json([
                    "msg"       => "Error, foreign key",
                    "id"        => $id
                ], 409);
            }
            else
            {
        		$record->deleted_at	= time();
        		$record->save();

        		if( $record->trashed() )
        		{
        			$response = response()->json([
    					"msg"		=> "Record deleted",
    					"id"		=> $id
    				], 200);
        		}
        		else
        		{
        			$response = response()->json([
    					"msg"		=> "Error",
    					"id"		=> $id
    				], 400);
        		}
            }
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	// Verify the user AUTH!
        try
        {
            if (! $usuario = JWTAuth::parseToken()->authenticate())
            {
                return response()->json([
                    "msg"       => "Not found",
                    "record"    => Array()
                ], 404);
            }
        }
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
        {
            return response()->json(['token_expired'], $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
        {
            return response()->json(['token_invalid'], $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\JWTException $e)
        {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        /*
    	$records = Documento::with("asunto", "estado", "municipalidad", "usuario_creo")
    							->whereRaw("", array())
    							->get();
    	*/

    	$records = Documento::select("documento.*", "documentoasignacion.id_usuario_asignado", "documentoasignacion.fecha_cierre",
                                    "documentoasignacion.fecha_recibido", "documentoasignacion.recibido", "documentoasignacion.visto", 
                                    "documentoasignacion.id AS id_asignacion")
								->join("documentoasignacion", "documentoasignacion.id_documento", "=", "documento.id")
								->whereRaw( "documentoasignacion.id_estado > 0 AND documentoasignacion.activo > 0 AND
                                            ( documentoasignacion.id_usuario_asignado = ? OR documento.id_usuario_creo = ? )", 
											array($usuario->id, $usuario->id) )
								->with("estado", "municipalidad", "usuario_creo", "asignacion")
                                ->groupBy("documento.id")
                                ->get();

        //Search if the user is Director
        $usuario->puesto;
        if( $usuario->puesto->director )
        {
            $recordsDirector = Documento::select("documento.*", "documentoasignacion.id_usuario_asignado", 
                                    "documentoasignacion.fecha_cierre",
                                    "documentoasignacion.fecha_recibido", "documentoasignacion.recibido", "documentoasignacion.visto", 
                                    "documentoasignacion.id AS id_asignacion")
                                ->join("documentoasignacion", "documentoasignacion.id_documento", "=", "documento.id")
                                ->join("usuario", "usuario.id", "=", "documentoasignacion.id_usuario_asignado")
                                ->whereRaw( "usuario.id_unidad = ?", 
                                            array($usuario->id_unidad) )
                                ->with("estado", "municipalidad", "usuario_creo")
                                ->groupBy("documento.id")
                                ->get()
                                ->toArray();
        }
        else
        {
            $recordsDirector = Array();
        }
        //////////////////////////////////

    	if( count($records) > 0 )
    	{
            foreach ($records as $key => $value)
            {
                if( $value->recibido == 0 && $value->id_usuario_asignado == $usuario->id )
                {
                    $asignacion = DocumentoAsignacion::find( $value->id_asignacion );
                    $asignacion->recibido = 1;
                    $asignacion->save();
                }
            }

    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray(),
                "director"  => $recordsDirector
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array(),
                "director"  => $recordsDirector
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
        // Verify the user AUTH!
        try
        {
            if (! $usuario = JWTAuth::parseToken()->authenticate())
            {
                return response()->json([
                    "msg"       => "Not found",
                    "record"    => Array()
                ], 404);
            }
        }
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
        {
            return response()->json(['token_expired'], $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
        {
            return response()->json(['token_invalid'], $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\JWTException $e)
        {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

    	$record = Documento::with("asignacion", "estado", "municipalidad", "usuario_creo", "observacion")->find( $id );

    	if( $record )
    	{
            $record->asignacion_activa   = false;

    		foreach ($record->asignacion as $keyAsignacion => $valueAsignacion)
    		{
    			$valueAsignacion->usuario_asignado;
                $valueAsignacion->usuario_asigno;
                if( $usuario->id == $valueAsignacion->id_usuario_asignado && $valueAsignacion->activo == 1 )
                {
                    $record->asignacion_activa           = true;
                    $valueAsignacion->asignacion_activa  = true;
                }

                if( $valueAsignacion->visto == 0 && $valueAsignacion->id_usuario_asignado == $usuario->id )
                {
                    $asignacion = DocumentoAsignacion::find( $valueAsignacion->id );
                    $asignacion->visto = 1;
                    $asignacion->save();
                }
    		}

            foreach ($record->observacion as $keyObservacion => $valueObservacion)
            {
                $valueObservacion->usuario;
            }

    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    public function showImage($documento)
    {
    	$record = Documento::find( $documento );

    	if( $record )
    	{
    		$mime 	= Storage::mimeType( $record->src_file );
	    	$file 	= Storage::get( $record->src_file );

	    	$response = response($file, 200)
	    	->header("Content-Type", $mime);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Verify the user AUTH!
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Not found",
					"record"	=> Array()
				], 404);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_documento_padre" => "integer|exists:documento,id",
            "id_usuario_asignado" => "required|integer|exists:usuario,id",
			"asunto" => "required|max:150",
            "contenido" => "required",
			"dias_respuesta" => "required|integer",
			"documento_referencia" => "string",
			"foto" => "mimes:pdf,doc,docx,xls,xlsx,txt",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		// Upload file to CloudStorage
		if( $request->file("foto") )
		{
			if( $request->file("foto")->isValid() )
			{
				$file_name 		= "documento_" . str_random(10) . "." . $request->file("foto")->getClientOriginalExtension();
				$file_original	= file_get_contents( $request->file("foto")->getRealPath() );
				Storage::put( $file_name, $file_original );
				//$request->file("photo")->move($destinationPath, $file_name);
			}
			else
			{
				return response()->json([
					"msg"		=> "Error, file not uploaded",
					"errors"	=> $validator->errors()
				], 400);
			}
		}
		else
		{
			$file_name = "";
		}

    	// Now insert
    	$record = new Documento();

        $record->id_documento_padre     = $request->id_documento_padre;
    	$record->id_estado 				= 1;
    	$record->id_usuario_creo	 	= $usuario->id;
        $record->asunto                 = $request->asunto;
    	$record->contenido 				= $request->contenido;
    	$record->correlativo 			= $request->correlativo;
    	$record->dias_respuesta 		= $request->dias_respuesta;
    	$record->documento_referencia	= $request->documento_referencia;
    	$record->fecha_referencia 		= $request->fecha_referencia;
    	$record->src_file				= $file_name;

    	if( $record->save() )
    	{
    		// Now insert assignation
            $asignacion = new DocumentoAsignacion();

            $asignacion->id_documento           = $record->id;
            $asignacion->id_estado				= $record->id_estado;
            $asignacion->id_usuario_asignado    = $request->id_usuario_asignado;
            $asignacion->id_usuario_asigno      = $usuario->id;
            $asignacion->activo                 = 1;

            if( $asignacion->save() )
            {
            	// Now insert notification
	            $notificacion = new Notificacion();

	            $notificacion->id_documento		    = $record->id;
                $notificacion->id_tiponotificacion  = 1;
	            $notificacion->id_usuario 		    = $request->id_usuario_asignado;
	            $notificacion->save();

                $notificacion->documento;
                $notificacion->usuario;

                $redis = LRedis::connection();
                $redis->publish("new:notification", $notificacion->toJson());

                $response = response()->json([
                    "msg"       => "Success",
                    "record"    => $record->toArray()
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "record"    => Array()
                ], 400);
            }
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"asunto" => "required|max:150",
            "contenido" => "required",
			"correlativo" => "integer",
			"dias_respuesta" => "required|integer",
			"documento_referencia" => "string",
			"fecha_referencia" => "date",
			"foto" => "mimes:jpeg,bmp,png",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		// Upload file to CloudStorage
		if( $request->hasFile("foto") )
		{
			if( $request->file("foto")->isValid() )
			{
				$file_name = "documento_" . str_random(10) . "." . $request->file("foto")->getClientOriginalExtension();
				$file_original	= file_get_contents( $request->file("foto")->getRealPath() );
				Storage::put( $file_name, $file_original );
				//$request->file("photo")->move($destinationPath, $file_name);
			}
			else
			{
				return response()->json([
					"msg"		=> "Error, file not uploaded",
					"errors"	=> $validator->errors()
				], 400);
			}
		}

    	// Get the record correspond to $id
    	$record = Documento::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
	    	$record->id_estado 				= $request->id_estado;
	    	$record->id_usuario_creo 		= $request->id_usuario_creo;
            $record->asunto                 = $request->asunto;
	    	$record->contenido 				= $request->contenido;
	    	$record->correlativo 			= $request->input("correlativo", $record->correlativo);
	    	$record->dias_respuesta 		= $request->input("dias_respuesta", $record->dias_respuesta);
	    	$record->documento_referencia	= $request->documento_referencia;
	    	$record->fecha_referencia 		= $request->fecha_referencia;
	    	$record->src_file				= $request->hasFile("foto")? $file_name : $record->src_file;

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
