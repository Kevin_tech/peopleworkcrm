<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Notificacion;

class NotificacionController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth");
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = Notificacion::find( $id );

    	if( $record )
    	{
    		$record->deleted_at	= time();
    		$record->save();

    		if( $record->trashed() )
    		{
    			$response = response()->json([
					"msg"		=> "Record deleted",
					"id"		=> $id
				], 200);
    		}
    		else
    		{
    			$response = response()->json([
					"msg"		=> "Error",
					"id"		=> $id
				], 400);
    		}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Not found",
					"record"	=> Array()
				], 404);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	$records = Notificacion::with("documento", "tipo")
    							->where("id_usuario", $usuario->id)
    							->orderBy("created_at", "desc")
    							->get();

    	if( count($records) > 0 )
    	{
    		foreach ($records as $keyNotificacion => $valueNotificacion)
    		{
    			if( $valueNotificacion->id_documento > 0 )
    				$valueNotificacion->documento->asunto;
    			elseif( $valueNotificacion->id_oficio > 0 )
    				$valueNotificacion->oficio->asunto;
    		}

    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = Notificacion::find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_documento" => "required",
			"id_oficio" => "required",
			"id_tiponotificacion" => "required",
            "id_usuario" => "required",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new Notificacion();

    	$record->id_documento          = $request->id_documento;
    	$record->id_oficio             = $request->id_oficio;
        $record->id_tiponotificacion   = $request->id_tiponotificacion;
    	$record->id_usuario            = $request->id_usuario;

    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"visto" => "required",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = Notificacion::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->visto 	= $request->visto;

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
