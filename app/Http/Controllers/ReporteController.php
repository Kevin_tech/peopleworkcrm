<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use PDF;
use Storage;

use App\BoletaAutorizacion;
use App\Documento;
use App\DocumentoAsignacion;
use App\FirmaElectronica;
use App\Procedimiento;
use App\PagoCheque;
use App\User;

class ReporteController extends Controller
{
	public function reporteBoletaAutorizacion( Request $request )
    {
    	$data = BoletaAutorizacion::with("partida_presupuestaria",
    										"puesto",
    										"renglon",
    										"tipo_servicio",
    										"unidad")
    				->find( $request->id );

		if( $data )
	    {
	    	// HASH for Digital Sign
	    	$digital_signature = Hash::make( "RBA-ITINFOM-2016-" . $data->toJson() );

	    	$pdf = PDF::loadView("report.boletaautorizacion", array( "data" => $data, "digital_signature" => $digital_signature ))
	    					->setPaper( "letter" )->setOrientation("portrait");

			return $pdf->download("Boleta Autorizacion - " . $data->id . ".pdf");
		}
		else
		{
			return response("ERROR 404: NOT FOUND", 404);
		}
    }

    public function reporteDocumento( Request $request )
    {
    	$data = Documento::with("asignacion", "estado", "usuario_creo")->where("correlativo", $request->correlativo)->first();

		if( $data )
	    {
	    	$data->usuario_creo->unidad;

	    	foreach ($data->asignacion as $keyAsignacion => $valueAsignacion)
	    	{
	    		$valueAsignacion->estado;
	    		$valueAsignacion->usuario_asignado;
	    		$valueAsignacion->usuario_asignado->unidad;
	    		$valueAsignacion->usuario_asignado->unidad->director;
	    		$valueAsignacion->usuario_asigno;
	    	}

	    	// HASH for Digital Sign
	    	$digital_signature = Hash::make( "RD-ITINFOM-2016-" . $data->toJson() );

	    	$pdf = PDF::loadView("report.documento", array( "data" => $data, "digital_signature" => $digital_signature ))
	    					->setPaper( array(0,0,612.00,935.00) )->setOrientation("portrait");

	    	// SAVE file to CloudStorage
			if( $pdf )
			{
				$file_name 		= "reporteDocumento_" . str_random(10) . ".pdf";
				$file_original	= $pdf->output();
				Storage::put( $file_name, $file_original );

				$firmaelectronica 				= new FirmaElectronica();
				$firmaelectronica->contenido	= "RD-ITINFOM-2016-" . $data->toJson();
				$firmaelectronica->firma 		= $digital_signature;
				$firmaelectronica->src_file		= $file_name;
				$firmaelectronica->view			= "report.documento";
				$firmaelectronica->save();
			}

			return $pdf->download("Documento " . $request->correlativo . ".pdf");
		}
		else
		{
			return response("ERROR 404: NOT FOUND", 404);
		}
    }

    public function reporteExpedientes( Request $request )
    {
    	$where = array("documento.id_estado" => "3", "documentoasignacion.activo" => "1");

    	if( $request->id_unidad )
			$where["usuario.id_unidad"] = $request->id_unidad;

		if( $request->id_usuario )
			$where["usuario.id"] = $request->id_usuario;

		if( $request->inicio && $request->fin )
		{
			$inicio = date("Y/m/d", strtotime($request->inicio)) . " 00:00:00";
			$fin = date("Y/m/d", strtotime($request->fin)) . " 23:59:59";
		}
		else
		{
			$inicio = date("Y/m/d") . " 00:00:00";
			$fin = date("Y/m/d") . " 23:59:59";
		}

		$data = Documento::select("documento.*", "documentoasignacion.id_usuario_asignado", "usuario.id_unidad")
								->join("documentoasignacion", "documentoasignacion.id_documento", "=", "documento.id")
								->join("usuario", "usuario.id", "=", "documentoasignacion.id_usuario_asignado")
								->where( $where )
								->whereBetween( "documento.created_at", array($inicio, $fin) )
								->with("asignacion", "estado", "usuario_creo")->get();

		foreach( $data as $keyDocumento => $valueDocumento )
	    {
	    	$valueDocumento->usuario_creo->unidad;
	    	
	    	foreach ($valueDocumento->asignacion as $keyAsignacion => $valueAsignacion)
	    	{
	    		$valueAsignacion->estado;
	    		$valueAsignacion->usuario_asignado;
	    		$valueAsignacion->usuario_asignado->unidad;
	    		$valueAsignacion->usuario_asigno;
	    	}
	    }

	    // HASH for Digital Sign
	    $digital_signature = Hash::make( "RD-ITINFOM-2016-" . $data->toJson() );

	    $pdf = PDF::loadView("report.expedientes", array( "data" => $data, "inicio" => $inicio, "fin" => $fin, 
	    													"digital_signature" => $digital_signature ))
	    			->setPaper( array(0,0,612.00,935.00) )
	    			->setOrientation("landscape");

	    // SAVE file to CloudStorage
		if( $pdf )
		{
			$file_name 		= "reporteExpedientes_" . str_random(10) . ".pdf";
			$file_original	= $pdf->output();
			Storage::put( $file_name, $file_original );

			$firmaelectronica 				= new FirmaElectronica();
			$firmaelectronica->contenido	= "RE-ITINFOM-2016-" . $data->toJson();
			$firmaelectronica->firma 		= $digital_signature;
			$firmaelectronica->src_file		= $file_name;
			$firmaelectronica->view			= "report.expedientes";
			$firmaelectronica->save();
		}

		return $pdf->download("Expedientes {$inicio} - {$fin}.pdf");
    }

    public function reporteFirmaElectronica( Request $request )
    {
    	$record = FirmaElectronica::where( "firma", $request->signature )->get()->first();

    	if( $record )
    	{
    		$mime 	= Storage::mimeType( $record->src_file );
	    	$file 	= Storage::get( $record->src_file );

	    	$response = response($file, 200)
	    	->header("Content-Type", $mime);
    	}
    	else
    	{
    		$response = response("ERROR 404: NOT FOUND", 404);
    	}

    	return $response;
    }

    public function reporteOperaciones( Request $request )
    {
    	$where = array();

    	if( $request->id_unidad )
			$where["usuario.id_unidad"] = $request->id_unidad;

		if( $request->id_usuario )
			$where["usuario.id"] = $request->id_usuario;

		if( $request->inicio && $request->fin )
		{
			$inicio = date("Y/m/d", strtotime($request->inicio)) . " 00:00:00";
			$fin = date("Y/m/d", strtotime($request->fin)) . " 23:59:59";
		}
		else
		{
			$inicio = date("Y/m/d") . " 00:00:00";
			$fin = date("Y/m/d") . " 23:59:59";
		}

		$data = User::with("unidad")->where( $where )->get();

		foreach( $data as $keyUsuario => $valueUsuario )
	    {
	    	$valueUsuario->documentoingresado =
	    		Documento::where("id_usuario_creo", $valueUsuario->id)
	    					->whereBetween( "created_at", array($inicio, $fin) )
	    					->count();
	    	$valueUsuario->documentotrasladado =
	    		DocumentoAsignacion::where( array("id_usuario_asigno" => $valueUsuario->id, "id_estado" => "5") )
	    					->whereBetween( "created_at", array($inicio, $fin) )
	    					->count();
	    	$valueUsuario->documentorecibido =
	    		DocumentoAsignacion::where("id_usuario_asignado", $valueUsuario->id)
	    					->whereBetween( "created_at", array($inicio, $fin) )
	    					->count();
	    	$valueUsuario->documentoproceso =
	    		DocumentoAsignacion::whereRaw( "id_usuario_asignado = ? AND id_estado != ?", array($valueUsuario->id, "3") )
	    					->whereBetween( "created_at", array($inicio, $fin) )
	    					->count();
	    	$valueUsuario->documentofin =
	    		DocumentoAsignacion::whereRaw( "id_usuario_asignado = ? AND fecha_cierre != ?", array($valueUsuario->id, "0000-00-00 00:00:00") )
	    					->whereBetween( "created_at", array($inicio, $fin) )
	    					->count();
	    }

	    // HASH for Digital Sign
	    $digital_signature = Hash::make( "RD-ITINFOM-2016-" . $data->toJson() );

	    $pdf = PDF::loadView("report.operaciones", array( "data" => $data, "inicio" => $inicio, "fin" => $fin, 
	    													"digital_signature" => $digital_signature ))
	    			->setPaper( array(0,0,612.00,935.00) )
	    			->setOrientation("landscape");

	    // SAVE file to CloudStorage
		if( $pdf )
		{
			$file_name 		= "reporteOperaciones_" . str_random(10) . ".pdf";
			$file_original	= $pdf->output();
			Storage::put( $file_name, $file_original );

			$firmaelectronica 				= new FirmaElectronica();
			$firmaelectronica->contenido	= "RO-ITINFOM-2016-" . $data->toJson();
			$firmaelectronica->firma 		= $digital_signature;
			$firmaelectronica->src_file		= $file_name;
			$firmaelectronica->view			= "report.operaciones";
			$firmaelectronica->save();
		}

		return $pdf->download("Operaciones {$inicio} - {$fin}.pdf");
    }

    public function reporteProcedimiento( Request $request )
    {
    	$data = Procedimiento::with("actividad")->find( $request->id );

		if( $data )
	    {
	    	// HASH for Digital Sign
	    	$digital_signature = Hash::make( "RP-ITINFOM-2016-" . $data->toJson() );

	    	$pdf = PDF::loadView("report.procedimiento", array( "data" => $data, "digital_signature" => $digital_signature ))
	    					->setPaper( "letter" )->setOrientation("portrait");

	    	// SAVE file to CloudStorage
	    	/*
			if( $pdf )
			{
				$file_name 		= "reporteDocumento_" . str_random(10) . ".pdf";
				$file_original	= $pdf->output();
				Storage::put( $file_name, $file_original );

				$firmaelectronica 				= new FirmaElectronica();
				$firmaelectronica->contenido	= "RD-ITINFOM-2016-" . $data->toJson();
				$firmaelectronica->firma 		= $digital_signature;
				$firmaelectronica->src_file		= $file_name;
				$firmaelectronica->view			= "report.procedimiento";
				$firmaelectronica->save();
			}
			*/

			return $pdf->download("Procedimiento - " . $data->titulo . ".pdf");
		}
		else
		{
			return response("ERROR 404: NOT FOUND", 404);
		}
    }

    public function reportePagoCheque( Request $request )
    {
    	$data = PagoCheque::with("renglon", "unidad")->find( $request->id );

		if( $data )
	    {
	    	// HASH for Digital Sign
	    	$digital_signature = Hash::make( "RPC-ITINFOM-2016-" . $data->toJson() );

	    	$pdf = PDF::loadView("report.pagocheque", array( "data" => $data, "digital_signature" => $digital_signature ))
	    					->setPaper( "letter" )->setOrientation("portrait");

			return $pdf->download("Pago con cheque - " . $data->numero_dpi . ".pdf");
		}
		else
		{
			return response("ERROR 404: NOT FOUND", 404);
		}
    }
}
