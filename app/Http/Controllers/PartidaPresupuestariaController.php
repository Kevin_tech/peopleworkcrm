<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PartidaPresupuestaria;

class PartidaPresupuestariaController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth");
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = PartidaPresupuestaria::find( $id );

    	if( $record )
    	{
    		$record->deleted_at	= time();
    		$record->save();

    		if( $record->trashed() )
    		{
    			$response = response()->json([
					"msg"		=> "Record deleted",
					"id"		=> $id
				], 200);
    		}
    		else
    		{
    			$response = response()->json([
					"msg"		=> "Error",
					"id"		=> $id
				], 400);
    		}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	$records = PartidaPresupuestaria::with("fuente_financiamiento", "renglon")
    					->get();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = PartidaPresupuestaria::with("fuente_financiamiento", "renglon")
    					->find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
            "id_fuente_financiamiento" 		=> "required|integer|exists:rrhh_fuentefinanciamiento,id",
            "id_renglon" 					=> "required|integer|exists:rrhh_renglon,id",
			"actividad" 					=> "required|integer",
			"ejercicio_fiscal" 				=> "required|integer",
			"entidad" 						=> "required|integer",
			"obra" 							=> "required|integer",
			"programa" 						=> "required|integer",
			"proyecto" 						=> "required|integer",
			"sub_programa" 					=> "required|integer",
			"ubicacion" 					=> "required|integer",
			"unidad_desconcentrada" 		=> "required|integer",
			"unidad_ejecutora" 				=> "required|integer",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new PartidaPresupuestaria();

    	$record->id_fuente_financiamiento  	= $request->id_fuente_financiamiento;
        $record->id_renglon 		        = $request->id_renglon;
    	$record->actividad 		        	= $request->actividad;
    	$record->ejercicio_fiscal 		    = $request->ejercicio_fiscal;
    	$record->entidad 		        	= $request->entidad;
    	$record->obra 			        	= $request->obra;
    	$record->programa 		        	= $request->programa;
    	$record->proyecto 		       	 	= $request->proyecto;
    	$record->sub_programa 		       	= $request->sub_programa;
    	$record->ubicacion 		        	= $request->ubicacion;
    	$record->unidad_desconcentrada 		= $request->unidad_desconcentrada;
    	$record->unidad_ejecutora 		    = $request->unidad_ejecutora;

    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_fuente_financiamiento" 		=> "required|integer|exists:rrhh_fuentefinanciamiento,id",
            "id_renglon" 					=> "required|integer|exists:rrhh_renglon,id",
			"actividad" 					=> "required|integer",
			"ejercicio_fiscal" 				=> "required|integer",
			"entidad" 						=> "required|integer",
			"obra" 							=> "required|integer",
			"programa" 						=> "required|integer",
			"proyecto" 						=> "required|integer",
			"sub_programa" 					=> "required|integer",
			"ubicacion" 					=> "required|integer",
			"unidad_desconcentrada" 		=> "required|integer",
			"unidad_ejecutora" 				=> "required|integer",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = PartidaPresupuestaria::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
	    	$record->id_fuente_financiamiento  	= $request->input("id_fuente_financiamiento", $record->id_fuente_financiamiento);
	    	$record->id_renglon		        	= $request->input("id_renglon", $record->id_renglon);
	    	$record->actividad 		        	= $request->input("actividad", $record->actividad);
	    	$record->ejercicio_fiscal 		    = $request->input("ejercicio_fiscal", $record->ejercicio_fiscal);
	    	$record->entidad 		        	= $request->input("entidad", $record->entidad);
	    	$record->obra 			        	= $request->input("obra", $record->obra);
	    	$record->programa 		        	= $request->input("programa", $record->programa);
	    	$record->proyecto 		        	= $request->input("proyecto", $record->proyecto);
	    	$record->sub_programa 		        = $request->input("sub_programa", $record->sub_programa);
	    	$record->ubicacion 		        	= $request->input("ubicacion", $record->ubicacion);
	    	$record->unidad_desconcentrada 		= $request->input("unidad_desconcentrada", $record->unidad_desconcentrada);
	    	$record->unidad_ejecutora 		    = $request->input("unidad_ejecutora", $record->unidad_ejecutora);

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
