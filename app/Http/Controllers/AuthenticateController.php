<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Hash;
use Validator;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\UsuarioAcademica;
use App\UsuarioEmergencia;
use App\UsuarioHijo;
use App\UsuarioPadre;

class AuthenticateController extends Controller
{
	public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth", ["except" => ["authenticate"]]);
	}

	public function activateUser( $id )
	{
		// Get the record correspond to $id
    	$record = User::find( $id );

    	if( $record )
    	{
    		$record->activo = 1;

    		if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
	}

	public function authenticate(Request $request)
    {
    	$credentials 	= $request->only("usuario", "password");
    	//$claims			= ["foo" => "bar", "baz" => "bob"];

		try
		{
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials))
			{
				return response()->json(["error" => "Invalid credentials"], 401);
			}
		}
		catch (JWTException $e)
		{
			// something went wrong
			return response()->json(["error" => "Not authenticated"], 500);
		}

		// if no errors are encountered we can return a JWT
		return response()->json(compact("token"));
    }

    public function deactivateUser( $id )
	{
		// Get the record correspond to $id
    	$record = User::find( $id );

    	if( $record )
    	{
    		$record->activo = 0;

    		if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
	}

    /*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = User::with("oficioasignado", "oficiocreado", "oficiovobo", "documentoasignado", "documentocreado")->find( $id );

    	if( $record )
    	{
    		if( count( $record->oficioasignado ) > 0 || count( $record->oficiocreado ) > 0 || count( $record->oficiovobo ) > 0 
    			|| count( $record->documentoasignado ) > 0 || count( $record->documentocreado ) > 0 )
            {
                $response = response()->json([
                    "msg"       => "Error, foreign key",
                    "id"        => $id
                ], 409);
            }
            else
            {
	    		$record->deleted_at	= time();
	    		$record->save();

	    		if( $record->trashed() )
	    		{
	    			$response = response()->json([
						"msg"		=> "Record deleted",
						"id"		=> $id
					], 200);
	    		}
	    		else
	    		{
	    			$response = response()->json([
						"msg"		=> "Error",
						"id"		=> $id
					], 400);
	    		}
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }

    // somewhere in your controller
	public function getAuthenticatedUser()
	{
		try
		{
			if (! $user = JWTAuth::parseToken()->authenticate())
			{
				return response()->json(['user_not_found'], 404);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		// the token is valid and we have found the user via the sub claim
		$user->modulos;
		$user->unidad;
		$user->profesion;
		$user->puesto;

		foreach ($user->modulos as $keyModulos => $valueModulos)
		{
			$valueModulos->modulo;
		}

		$response = response()->json([
			"msg"		=> "Success",
			"record"	=> $user
		], 200);

		return $response;
	}

    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	$records = User::with("unidad", "profesion", "puesto")->get();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** RESET USER PASSWORD WITH JWT TOKEN VALIDATE
    */
    public function resetUserPassword( $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		if( $usuario->admin == 1 )
		{
	    	$record = User::find( $id );

	    	if( $record )
	    	{
	    		$new_password = "secreto." . $record->usuario;
	    		$record->password = bcrypt( $new_password );

	    		if( $record->save() )
		    	{
		    		$response = response()->json([
						"msg"		=> "Success",
						"password"	=> $new_password
					], 200);
		    	}
		    	else
		    	{
		    		$response = response()->json([
						"msg"		=> "Error",
						"password"	=> ""
					], 400);
		    	}
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Not found",
					"password"	=> ""
				], 404);
	    	}
	    }
	    else
    	{
    		$response = response()->json([
				"msg"		=> "Forbidden",
				"password"	=> ""
			], 403);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	$record = User::with("unidad", "documentoasignado", "documentoasigno", "documentocreado", "profesion", "puesto")->find( $id );

    	if( $record )
    	{
    		$record->you = $record->id == $usuario->id;
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"id_estado_civil" 					=> "required_with:rrhh|integer|exists:rrhh_estadocivil,id",
    		"id_municipio" 						=> "required_with:rrhh|integer|exists:municipio,id",
    		"id_municipio_conyugue" 			=> "required_with:conyugue_nombre|integer|exists:municipio,id",
    		"id_municipio_conyugue_residencia" 	=> "required_with:conyugue_nombre|integer|exists:municipio,id",
    		"id_municipio_residencia" 			=> "required_with:rrhh|integer|exists:municipio,id",
			"id_unidad" 						=> "required_without:rrhh|integer|exists:unidad,id",
			"id_profesion" 						=> "required_without:rrhh|integer|exists:profesion,id",
			"id_puesto" 						=> "required_without:rrhh|integer|exists:puesto,id",

			"admin" 							=> "boolean",
			"apellido_1" 						=> "required|max:50",
			"apellido_2" 						=> "max:50",
			"apellido_3" 						=> "max:50",

			"colegiado_activo"					=> "boolean",
			"colegiado_colegio" 				=> "required_with:colegiado_activo",
			"colegiado_constancia" 				=> "required_with:colegiado_activo|max:20",
			"colegiado_numero" 					=> "required_with:colegiado_activo|max:20",

			"conyugue_celular" 					=> "max:10",
			"conyugue_direccion" 				=> "required_with:conyugue_nombre",
			"conyugue_dpi" 						=> "required_with:conyugue_nombre|max:20",
			"conyugue_fecha_nacimiento" 		=> "required_with:conyugue_nombre",
			"conyugue_nit" 						=> "max:20",
			"conyugue_nombre" 					=> "max:200",
			"conyugue_telefono" 				=> "max:10",

			"email_laboral" 					=> "max:25",
			"email_personal" 					=> "max:25",
			"fecha_nacimiento"					=> "required_with:rrhh",
			"foto"								=> "mimes:jpeg,bmp,png",
			"nit" 								=> "max:15",

			"nombre_1" 							=> "required|max:50",
			"nombre_2" 							=> "max:50",
			"nombre_3" 							=> "max:50",

			"numero_dpi" 						=> "required_with:rrhh|max:20|unique:usuario,numero_dpi",
			"numero_igss" 						=> "max:20",
			"numero_licencia_1" 				=> "max:20",
			"numero_licencia_2" 				=> "max:20",
			"numero_probidad" 					=> "max:20",

			"password" 							=> "required_without:rrhh|max:50",
			"residencia_direccion" 				=> "required_with:rrhh",
			"residencia_telefono" 				=> "max:10",
			"rrhh" 								=> "boolean",
			"sexo" 								=> "required_with:rrhh|boolean",
			"usuario" 							=> "required|max:25|unique:usuario,usuario",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"request"	=> $request->all(),
				"errors"	=> $validator->errors()
			], 400);
		}

		// Begin transaction!
		DB::beginTransaction();

    	// Now insert
    	$record = new User();
    	$secretopwd = "secreto." . $request->usuario;

    	$record->id_estado_civil 					= $request->id_estado_civil;
    	$record->id_municipio 						= $request->id_municipio;
    	$record->id_municipio_conyugue 				= $request->id_municipio_conyugue;
    	$record->id_municipio_conyugue_residencia 	= $request->id_municipio_conyugue_residencia;
    	$record->id_municipio_residencia 			= $request->id_municipio_residencia;
    	$record->id_unidad 							= $request->id_unidad;
    	$record->id_profesion 						= $request->id_profesion;
    	$record->id_puesto 							= $request->id_puesto;
    	$record->admin 								= $request->admin? 1 : 0;
    	$record->apellido_1 						= $request->apellido_1;
    	$record->apellido_2 						= $request->apellido_2;
    	$record->apellido_3 						= $request->apellido_3;

    	$record->colegiado_activo 					= $request->colegiado_activo? 1 : 0;
    	$record->colegiado_colegio 					= $request->colegiado_colegio;
    	$record->colegiado_constancia				= $request->colegiado_constancia;
    	$record->colegiado_numero 					= $request->colegiado_numero;

    	$record->conyugue_celular 					= $request->conyugue_celular;
    	$record->conyugue_direccion 				= $request->conyugue_direccion;
    	$record->conyugue_dpi 						= $request->conyugue_dpi;
    	$record->conyugue_fecha_nacimiento 			= $request->conyugue_fecha_nacimiento;
    	$record->conyugue_nit 						= $request->conyugue_nit;
    	$record->conyugue_nombre 					= $request->conyugue_nombre;
    	$record->conyugue_telefono 					= $request->conyugue_telefono;

    	$record->email_laboral 						= $request->email_laboral;
    	$record->email_personal 					= $request->email_personal;
    	$record->fecha_nacimiento 					= $request->fecha_nacimiento;
    	$record->nit 								= $request->nit;

    	$record->nombre_1 							= $request->nombre_1;
    	$record->nombre_2 							= $request->nombre_2;
    	$record->nombre_3 							= $request->nombre_3;

    	$record->numero_dpi 						= $request->numero_dpi;
    	$record->numero_igss 						= $request->numero_igss;
    	$record->numero_licencia_1 					= $request->numero_licencia_1;
    	$record->numero_licencia_2 					= $request->numero_licencia_2;
    	$record->numero_probidad 					= $request->numero_probidad;

    	$record->password 							= bcrypt( $request->input("password", $secretopwd) );
    	$record->residencia_direccion 				= $request->residencia_direccion;
    	$record->residencia_telefono 				= $request->residencia_telefono;
    	$record->sexo 								= $request->sexo? 1 : 0;
    	$record->usuario 							= $request->usuario;

    	if( $record->save() )
    	{
    		// Now, if it's module of RRHH then proceed to insert the "hijos, padres, emergencia, colegios" data
    		if( $request->rrhh )
    		{
    			//UsuarioAcademica
    			$json_informacion_academica = json_decode( $request->json_informacion_academica );
    			foreach ($json_informacion_academica as $keyAcademica => $valueAcademica)
    			{
    				$recordAcademica 					= new UsuarioAcademica();
    				$recordAcademica->id_nivel_academico= $valueAcademica->id_nivel_academico;
    				$recordAcademica->id_usuario 		= $record->id;
    				$recordAcademica->establecimiento 	= $valueAcademica->establecimiento;
    				$recordAcademica->fecha 			= $valueAcademica->fecha;
    				$recordAcademica->titulo 			= $valueAcademica->titulo;
    				$recordAcademica->save();

    				if( !$recordAcademica->save() )
    				{
    					// Oops, something it's bad :(
			    		DB::rollBack();

			    		$response = response()->json([
							"msg"		=> "Informacion academica incorrecta",
							"record"	=> Array()
						], 400);
    				}
    			}

    			//UsuarioEmergencia
    			$json_informacion_emergencia = json_decode( $request->json_informacion_emergencia );
    			foreach ($json_informacion_emergencia as $keyEmergencia => $valueEmergencia)
    			{
    				$recordEmergencia 					= new UsuarioEmergencia();
    				$recordEmergencia->id_usuario 		= $record->id;
    				$recordEmergencia->direccion 		= $valueEmergencia->direccion;
    				$recordEmergencia->nombre 			= $valueEmergencia->nombre;
    				$recordEmergencia->parentesco 		= $valueEmergencia->parentesco;
    				$recordEmergencia->telefono 		= $valueEmergencia->telefono;
    				
    				if( !$recordEmergencia->save() )
    				{
    					// Oops, something it's bad :(
			    		DB::rollBack();

			    		$response = response()->json([
							"msg"		=> "Informacion de emergencia incorrecta",
							"record"	=> Array()
						], 400);
    				}
    			}

    			//UsuarioHijo
    			$json_hijos = json_decode( $request->json_hijos );
    			foreach ($json_hijos as $keyHijo => $valueHijo)
    			{
    				$recordHijo 					= new UsuarioHijo();
    				$recordHijo->id_usuario 		= $record->id;
    				$recordHijo->fecha_nacimiento 	= $valueHijo->fecha_nacimiento;
    				$recordHijo->nombre 			= $valueHijo->nombre;
    				$recordHijo->sexo 				= $valueHijo->sexo;
    				$recordHijo->save();

    				if( !$recordHijo->save() )
    				{
    					// Oops, something it's bad :(
			    		DB::rollBack();

			    		$response = response()->json([
							"msg"		=> "Informacion de hijos incorrecta",
							"record"	=> Array()
						], 400);
    				}
    			}

    			//UsuarioPadre
    			$json_padres = json_decode( $request->json_padres );
    			foreach ($json_padres as $keyPadre => $valuePadre)
    			{
    				$recordPadre 					= new UsuarioPadre();
    				$recordPadre->id_estado_civil 	= $valuePadre->id_estado_civil;
    				$recordPadre->id_usuario 		= $record->id;
    				$recordPadre->direccion 		= $valuePadre->direccion;
    				$recordPadre->nombre 			= $valuePadre->nombre;
    				$recordPadre->sexo 				= $valuePadre->sexo;
    				$recordPadre->telefono 			= $valuePadre->telefono;
    				$recordPadre->save();

    				if( !$recordPadre->save() )
    				{
    					// Oops, something it's bad :(
			    		DB::rollBack();

			    		$response = response()->json([
							"msg"		=> "Informacion de padres incorrecta",
							"record"	=> Array()
						], 400);
    				}
    			}
    		}

    		// All it's ok, commit :)
    		DB::commit();

    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		// Oops, something it's bad :(
    		DB::rollBack();

    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
		// Validator first!
    	$validator = Validator::make($request->all(), [
			"id_unidad" => "required|integer|exists:unidad,id",
			"id_profesion" => "required|integer|exists:profesion,id",
			"id_puesto" => "required|integer|exists:puesto,id",
			"apellido_1" => "required|max:50",
			"apellido_2" => "required|max:50",
			"nombre_1" => "required|max:50",
			"nombre_2" => "required|max:50"
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = User::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->id_unidad			= $request->id_unidad;
	    	$record->id_profesion 		= $request->id_profesion;
	    	$record->id_puesto 			= $request->id_puesto;
	    	$record->apellido_1 		= $request->apellido_1;
	    	$record->apellido_2 		= $request->apellido_2;
	    	$record->nombre_1 			= $request->nombre_1;
	    	$record->nombre_2 			= $request->nombre_2;

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** UPDATE USER PASSWORD WITH JWT TOKEN VALIDATE
    */
    public function updateUserPassword( Request $request )
    {
    	try
		{
			if (! $record = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		// Validator first!
    	$validator = Validator::make($request->all(), [
			"oldpassword" => "required|max:50",
			"password" => "required|max:50|confirmed",
			"password_confirmation" => "required|max:50",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		if( Hash::check($request->oldpassword, $record->password) )
		{
			$record->password 			= bcrypt( $request->password );

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
		}
		else
		{
			$response = response()->json([
				"msg"		=> "Current password not match",
				"record"	=> Array()
			], 400);
		}
    	
    	return $response;
    }

    /*
	** UPDATE USER PASSWORD WITH USER ID (ADMIN ONLY)
    */
    public function updateUserPasswordAdmin( Request $request, $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"password" => "required|max:50|confirmed",
			"password_confirmation" => "required|max:50",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		if( $usuario->admin == 1 )
		{
	    	// Get the record correspond to $id
	    	$record = User::find( $id );

	    	if( $record )
	    	{
	    		$record->password = $request->input("password", bcrypt( $record->password ));

	    		if( $record->save() )
		    	{
		    		$response = response()->json([
						"msg"		=> "Success",
						"record"	=> $record->toArray()
					], 200);
		    	}
		    	else
		    	{
		    		$response = response()->json([
						"msg"		=> "Error",
						"record"	=> Array()
					], 400);
		    	}
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Not found",
					"record"	=> Array()
				], 404);
	    	}
	    }
	    else
    	{
    		$response = response()->json([
				"msg"		=> "Forbidden",
				"record"	=> Array()
			], 403);
    	}

    	return $response;
    }
}
