<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    //
    use SoftDeletes;

    protected $table = 'municipio';
    protected $dates = ['deleted_at'];
}
