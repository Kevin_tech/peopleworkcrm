<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notificacion extends Model
{
    //
	use SoftDeletes;

    protected $table = 'notificacion';
    protected $dates = ['deleted_at'];

    public function documento()
    {
        return $this->belongsTo("App\Documento", "id_documento");
    }

    public function oficio()
    {
        return $this->belongsTo("App\Oficio", "id_oficio");
    }

    public function tipo()
    {
        return $this->belongsTo("App\TipoNotificacion", "id_tiponotificacion");
    }

    public function usuario()
    {
        return $this->belongsTo("App\User", "id_usuario");
    }
}
