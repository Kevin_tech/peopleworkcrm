var app = angular.module("appTracking", [ "angular-loading-bar", "angularMoment", "btford.socket-io", "file-model", "ngDialog", "ngResource", "ngRoute", "ngSanitize", "ngStorage", "oitozero.ngSweetAlert", "omr.directives", "simplePagination" ])

.config(["$routeProvider", "$httpProvider", "cfpLoadingBarProvider",
function ($routeProvider, $httpProvider, cfpLoadingBarProvider)
{
	$routeProvider
	.when("/administracion",
	{
		controller: "AdministracionController",
		listview: true,
		templateUrl: "module/administracion",
		title: "Administracion del Sistema"
	})
		.when("/unidades/new",
		{
			controller: "AdministracionController",
			newform: true,
			templateUrl: "module/administracion",
			title: "Nueva unidad"
		})
		.when("/unidades/:id",
		{
			controller: "AdministracionController",
			editform: true,
			templateUrl: "module/administracion",
			title: "Editar unidad"
		})
		.when("/puestos/new",
		{
			controller: "AdministracionController",
			newform: true,
			templateUrl: "module/administracion",
			title: "Nuevo puesto"
		})
		.when("/puestos/:id",
		{
			controller: "AdministracionController",
			editform: true,
			templateUrl: "module/administracion",
			title: "Editar puesto"
		})
	.when("/documentos",
	{
		controller: "DocumentoController",
		listview: true,
		templateUrl: "module/documentos",
		title: "Mis Documentos"
	})
		.when("/documentos/new",
		{
			controller: "DocumentoController",
			newform: true,
			templateUrl: "module/documentos",
			title: "Nuevo documento"
		})
		.when("/documentos/:id",
		{
			controller: "DocumentoController",
			article: true,
			templateUrl: "article/documento",
			title: "Documento"
		})
	.when("/home",
	{
		controller: "HomeController",
		templateUrl: "module/dashboard",
		title: "Inicio"
	})
	/*
	=============================================================
	 !ALL OF "OFICIOS" IT'S DEPRECATED, NOW ARE "DOCUMENTOS" ONY
	=============================================================
	.when("/oficios",
	{
		controller: "OficioController",
		listview: true,
		templateUrl: "module/oficios",
		title: "Mis Oficios"
	})
		.when("/oficios/new",
		{
			controller: "OficioController",
			newform: true,
			templateUrl: "module/oficios",
			title: "Nuevo oficio"
		})
		.when("/oficios/:id",
		{
			controller: "OficioController",
			article: true,
			templateUrl: "article/oficio",
			title: "Oficio"
		})
	*/
	.when("/pagos",
	{
		controller: "PagoController",
		listview: true,
		templateUrl: "module/pagos",
		title: "Pagos"
	})
		.when("/pagos/new",
		{
			controller: "PagoController",
			newform: true,
			templateUrl: "module/pagos",
			title: "Nuevo Pago"
		})
		.when("/pagos/:id",
		{
			controller: "PagoController",
			article: true,
			templateUrl: "article/pagocheque",
			title: "Pago"
		})
	.when("/procedimientos",
	{
		controller: "ProcedimientoController",
		listview: true,
		templateUrl: "module/procedimientos",
		title: "Procedimientos"
	})
		.when("/procedimientos/new",
		{
			controller: "ProcedimientoController",
			newform: true,
			templateUrl: "module/procedimientos",
			title: "Nuevo procedimiento"
		})
		.when("/procedimientos/:id",
		{
			controller: "ProcedimientoController",
			article: true,
			templateUrl: "article/procedimiento",
			title: "Procedimiento"
		})
		.when("/procedimientos/:id/edit",
		{
			controller: "ProcedimientoController",
			editform: true,
			templateUrl: "module/procedimientos",
			title: "Editar Procedimiento"
		})
	.when("/recursoshumanos",
	{
		controller: "RecursosHumanosController",
		listview: true,
		templateUrl: "module/recursoshumanos",
		title: "Recursos Humanos"
	})
		.when("/recursoshumanos/new",
		{
			controller: "RecursosHumanosController",
			newform: true,
			templateUrl: "module/recursoshumanos",
			title: "Registro de personal"
		})
	.when("/recursoshumanos/boletasautorizacion",
	{
		controller: "BoletaAutorizacionController",
		listview: true,
		templateUrl: "module/boletasautorizacion",
		title: "Mis Boletas de Autorizacion de Contratos"
	})
		.when("/recursoshumanos/boletasautorizacion/new",
		{
			controller: "BoletaAutorizacionController",
			newform: true,
			templateUrl: "module/boletasautorizacion",
			title: "Nueva Boleta de Autorización de Contratos"
		})
		.when("/recursoshumanos/boletasautorizacion/:id",
		{
			controller: "BoletaAutorizacionController",
			article: true,
			templateUrl: "article/boletaautorizacion",
			title: "Boleta de Autorización de Contrato"
		})
	.when("/recursoshumanos/fuentesfinanciamiento",
	{
		controller: "FuenteFinanciamientoController",
		listview: true,
		templateUrl: "module/fuentesfinanciamiento",
		title: "Mis Fuentes de Financiamiento"
	})
		.when("/recursoshumanos/fuentesfinanciamiento/new",
		{
			controller: "FuenteFinanciamientoController",
			newform: true,
			templateUrl: "module/fuentesfinanciamiento",
			title: "Nueva Fuente de Financiamiento"
		})
		.when("/recursoshumanos/fuentesfinanciamiento/:id",
		{
			controller: "FuenteFinanciamientoController",
			article: true,
			templateUrl: "article/fuentefinanciamiento",
			title: "Fuente de Financiamiento"
		})
	.when("/recursoshumanos/partidaspresupuestarias",
	{
		controller: "PartidaPresupuestariaController",
		listview: true,
		templateUrl: "module/partidaspresupuestarias",
		title: "Mis Partidas Presupuestarias"
	})
		.when("/recursoshumanos/partidaspresupuestarias/new",
		{
			controller: "PartidaPresupuestariaController",
			newform: true,
			templateUrl: "module/partidaspresupuestarias",
			title: "Nueva Partida Presupuestaria"
		})
		.when("/recursoshumanos/partidaspresupuestarias/:id",
		{
			controller: "PartidaPresupuestariaController",
			article: true,
			templateUrl: "article/partidapresupuestaria",
			title: "Partida Presupuestaria"
		})
	.when("/reportes",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportes",
		title: "Mis Reportes"
	})
	.when("/usuarios",
	{
		controller: "UsuarioController",
		listview: true,
		templateUrl: "module/usuarios",
		title: "Mis Usuarios"
	})
		.when("/usuarios/new",
		{
			controller: "UsuarioController",
			newform: true,
			templateUrl: "module/usuarios",
			title: "Nuevo usuario"
		})
		.when("/usuarios/:id",
		{
			controller: "UsuarioController",
			article: true,
			templateUrl: "article/usuario",
			title: "Perfil de Usuario"
		})
	.when("/logout",
	{
		controller: "LogoutController",
		templateUrl: "module/logout",
		title: "...Saliendo..."
	})
	.otherwise({ redirectTo: "/home" });

	$httpProvider.interceptors.push(["$q", "$location", "$localStorage", "SweetAlert", function ($q, $location, $localStorage, SweetAlert) 
	{
		return {
			"request": function (config)
			{
				console.log( "Data: " );
				console.log( config.data  );

				if ($localStorage.token === undefined)
				{
					$location.path("/logout");
				}

				config.headers = config.headers || {};
				if ($localStorage.token)
				{
					config.headers.Authorization = "Bearer " + $localStorage.token;
				}
				
				return config;
			},
			"responseError": function (response)
			{
				if( response.status === 400 )
				{
					var errors = null;

					if( response.data.errors != undefined )
						errors = Object.keys( response.data.errors )
									.toString()
									.replace(/id_/g, "")
									.replace(/_/g, " ");

					SweetAlert.swal("Error al guardar", "Revisa los siguientes campos: " + errors, "error");
				}
				else if (response.status === 401 || response.status === 403)
				{
					$location.path("/logout");
				}
				else if( response.status === -1 )
				{
					SweetAlert.swal("Sin conexión", "No es posible establecer conexión con el servidor", "error");
				}
				else
				{
					SweetAlert.swal("Error al guardar", "Revisa que no falte ningun dato importante y prueba de nuevo", "error");
				}

				console.log( response );

				return $q.reject(response);
			}
		};
	}]);

	$httpProvider.defaults.transformRequest = function(data)
	{
		if (data === undefined)
			return data;

		var fd = new FormData();
		angular.forEach(data, function(value, key)
		{
			if (value instanceof FileList)
			{
				if (value.length == 1)
				{
					fd.append(key, value[0]);
				}
				else
				{
					angular.forEach(value,
						function(file, index) 
						{
							fd.append(key + "_" + index, file);
						});
				}
			}
			else
			{
				fd.append(key, value);
			}
		});

		return fd;
	}

	$httpProvider.defaults.headers.post["Content-Type"] = undefined;

	cfpLoadingBarProvider.includeSpinner = false;
}])

.constant("urls",
{
	BASE: "/",
	BASE_API: "/api",
	BASE_REPORT: "/report",
	APP_TITLE: " Tracking ::: INFOM "
})

.controller("AdministracionController", ["$rootScope", "$scope", "$location", "$routeParams", "Unidad", "Institucion", "Puesto", "SweetAlert", "Usuario",
function($rootScope, $scope, $location, $routeParams, Unidad, Institucion, Puesto, SweetAlert, Usuario)
{
	$rootScope.currentModule = "administracion";
	$scope.currentSubModule = $location.path();
	$scope.current_date = new Date();
	$scope.unidad = {};
	$scope.puesto = {};
	$scope.regexUnidades = /^\/unidades\/[0-9+]/;
	$scope.regexPuestos = /^\/puestos\/[0-9+]/;

	if( $scope.listview )
	{
		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});

		Puesto.get(function(data)
		{
			$scope.puestos = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Unidad.get({id:id}, function(data)
		{
			$scope.unidad = data.record;
		});

		Puesto.get({id:id}, function(data)
		{
			$scope.puesto = data.record;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				if( $scope.currentSubModule == "/unidades/new" )
				{
					Unidad.save( $scope.unidad, function(data)
					{
						//alert( "Unidad creado" );
						SweetAlert.swal("Operación exitosa", "Unidad creado", "success");
						$location.path( "administracion" );
					});
				}

				else if( $scope.currentSubModule == "/puestos/new" )
				{
					Puesto.save( $scope.puesto, function(data)
					{
						SweetAlert.swal("Operación exitosa", "Puesto creado", "success");
						$location.path( "administracion" );
					});
				}
			}

			else if( $scope.editform )
			{
				if( $scope.regexUnidades.test( $scope.currentSubModule ) )
				{
					Unidad.update( $scope.unidad, function(data)
					{
						SweetAlert.swal("Operación exitosa", "Unidad modificada", "success");
						$location.path( "administracion" );
					});
				}

				else if( $scope.regexPuestos.test( $scope.currentSubModule ) )
				{
					Puesto.update( $scope.puesto, function(data)
					{
						SweetAlert.swal("Operación exitosa", "Unidad modificada", "success");
						$location.path( "administracion" );
					});
				}
			}
		}
}])

.controller("BoletaAutorizacionController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "urls", "BoletaAutorizacion", "FuenteFinanciamiento", "Pagination", "PartidaPresupuestaria", "Renglon", "SweetAlert", "TipoServicio", "Unidad",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, urls, BoletaAutorizacion, FuenteFinanciamiento, Pagination, PartidaPresupuestaria, Renglon, SweetAlert, TipoServicio, Unidad)
{
	$rootScope.currentModule 		= "recursoshumanos";
	$rootScope.currentSubModule		= "boletasautorizacion";
	$scope.new_boletaautorizacion	= {}
	$scope.pagination 				= Pagination.getNew(10);

	$scope.actualizarFuentesFinanciamiento = function()
		{
			FuenteFinanciamiento.get(function(data)
			{
				$scope.fuentesfinanciamiento = data.records;
			});
		}

	$scope.actualizarPartidasPresupuestarias = function()
		{
			PartidaPresupuestaria.get(function(data)
			{
				$scope.partidaspresupuestarias = data.records;
			});
		}

	$scope.actualizarRenglones = function()
		{
			Renglon.get(function(data)
			{
				$scope.renglones = data.records;
			});
		}

	$scope.actualizarTiposServicio = function()
		{
			TipoServicio.get(function(data)
			{
				$scope.tiposservicios = data.records;
			});
		}

	$scope.actualizarUnidades = function()
		{
			Unidad.get(function(data)
			{
				$scope.unidades = data.records;
			});
		}

	$scope.generateReportBoletaAutorizacion = function()
		{
			$window.open( urls.BASE_REPORT + "/boletaautorizacion?id=" + $scope.boletaautorizacion.id );
		}

	if( $scope.article )
	{
		var id = $routeParams.id
		
		BoletaAutorizacion.get({id:id}, function(data)
		{
			$scope.boletaautorizacion = data.record;
		});
	}

	if( $scope.listview )
	{
		BoletaAutorizacion.get(function(data)
		{
			$scope.boletasautorizacion = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		$scope.actualizarFuentesFinanciamiento();
		$scope.actualizarPartidasPresupuestarias();
		$scope.actualizarRenglones();
		$scope.actualizarTiposServicio();
		$scope.actualizarUnidades()

		$scope.submit = function()
		{
			if( $scope.newform )
			{
				$scope.new_boletaautorizacion.fecha_inicio 	= $filter("date")($scope.new_boletaautorizacion.fecha_inicio, "yyyy-MM-dd");
				$scope.new_boletaautorizacion.fecha_fin 	= $filter("date")($scope.new_boletaautorizacion.fecha_fin, "yyyy-MM-dd");
				console.log( $scope.new_boletaautorizacion );
				BoletaAutorizacion.save( $scope.new_boletaautorizacion, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Boleta registrada", "success");
					$location.path( "recursoshumanos/boletasautorizacion" );
				});
			}
		}
	}
}])

.controller("DocumentoController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "ngDialog", "urls", "Documento", "DocumentoAnulacion", "DocumentoAsignacion", "DocumentoObservacion", "Estado", "Pagination", "Sequence", "SubSequence", "SweetAlert", "Unidad", "Usuario",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, ngDialog, urls, Documento, DocumentoAnulacion, DocumentoAsignacion, DocumentoObservacion, Estado, Pagination, Sequence, SubSequence, SweetAlert, Unidad, Usuario)
{
	$rootScope.currentModule 	= "documentos";
	$scope.current_date 		= new Date();
	$scope.dateFormat 			= "";
	$scope.documentoLeido 		= true;

	$scope.new_documento =
	{
		id_documento_padre: 0,
		dias_respuesta: 0
	}

	$scope.pagination 			= Pagination.getNew(10);
	$scope.usuarioFiltro 		= { }
	$scope.usuarioReplyFiltro 	= { }

	if( $scope.article )
	{
		var id = $routeParams.id

		$scope.new_observacion =
		{
			id_documento: id
		};
		$scope.reAsignacion =
		{
			id_documento: id
		};
		$scope.usuarioReplyFiltro.id_unidad 	= $rootScope.me.id_unidad;
		$scope.usuarioReplyFiltro.id_puesto 	= "";
		
		Documento.get({id:id}, function(data)
		{
			$scope.documento = data.record;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	if( $scope.listview )
	{
		$scope.documentoFiltro =
		{
			id_estado: 1
		};

		Estado.get(function(data)
		{
			$scope.estados = data.records;
		});

		Documento.get(function(data)
		{
			$scope.documentos = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});

		Sequence.save({}, function(data)
		{
			var formatCreatedAt 			= $filter("amDateFormat")(data.record.created_at, "YYYY");
			var padSequence					= $filter("pad")(data.record.sequence, 6);
			
			$scope.currentSequence			= data.record;
			$scope.new_documento.correlativo 	= formatCreatedAt + "-" + padSequence;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	$scope.generateReportDocumento = function()
		{
			$window.open( urls.BASE_REPORT + "/documento?correlativo=" + $scope.documento.correlativo );
		}

	$scope.openFile = function()
		{
			$window.open( "/api/documento/" + $scope.documento.id + "/imagen?token=" + $localStorage.token );
		}

	$scope.pubObservacion = function()
		{
			DocumentoObservacion.save( $scope.new_observacion, function(data)
			{
				$scope.documento.observacion.push( data.record );
				$scope.new_observacion.mensaje = "";
			});
		}

	$scope.reply = function(asignacion)
		{
			DocumentoAsignacion.update( asignacion, function(data)
			{
				asignacion.id_estado		= data.record.id_estado;
				asignacion.fecha_cierre		= data.record.fecha_cierre;
				asignacion.fecha_recibido	= data.record.fecha_recibido;
				asignacion.mensaje_cierre	= data.record.mensaje_cierre;
				asignacion.mensaje_recibido	= data.record.mensaje_recibido;

				SweetAlert.swal("Operación exitosa", "Haz respondido a este documento", "success");
			});
		}

	$scope.showReplyWith = function()
		{
			Unidad.get(function(data)
			{
				$scope.unidades = data.records;
			});

			SubSequence.save({ correlativo: $scope.documento.correlativo }, function(data)
			{
				var formatCreatedAt 			= $filter("amDateFormat")(data.record.created_at, "YYYY");
				var padSequence					= $filter("pad")(data.record.sequence, 6);
				
				$scope.currentSequence			= data.record;
				$scope.new_documento.correlativo= formatCreatedAt + "-" + padSequence + "-" + data.record.sub_sequence;
			});

			$scope.newform = true;
			$scope.new_documento.id_documento_padre = $scope.documento.id;

			console.log( $scope.new_documento );
		}

	$scope.showSolicitudAnulacion = function()
		{
			ngDialog.open(
				{ 
					template: "form/solicitudanulacion", 
					className: "ngdialog-theme-default",
					controller: "DocumentoAnulacionController",
					data: $scope.documento
				});
		}

	$scope.submit = function()
		{
			var formatFechaReferencia 				= $filter("date")($scope.new_documento.mi_fecha_referencia, "yyyy-MM-dd");
			$scope.new_documento.fecha_referencia 	= formatFechaReferencia;

			if( $scope.newform )
			{
				Documento.save( $scope.new_documento, function(data)
				{
					console.log( data );
					SweetAlert.swal("Operación exitosa", "Documento creado", "success");
					$location.path( "documentos" );
				});
			}
		}

	$scope.transfer = function(reAsignacion)
		{
			DocumentoAsignacion.save( reAsignacion, function(data)
			{
				SweetAlert.swal("Operación exitosa", "El documento ha sido transferido", "success");
			});
		}
}])

.controller("DocumentoAnulacionController", ["$rootScope", "$scope", "DocumentoAnulacion", "SweetAlert",
function($rootScope, $scope, DocumentoAnulacion, SweetAlert)
{
	$scope.new_anulacion =
	{
		id_documento: $scope.ngDialogData.id
	};

	$scope.submitSolicitudAnulacion = function()
		{
			DocumentoAnulacion.save( $scope.new_anulacion, function(data)
			{
				SweetAlert.swal("Operación exitosa", "Solicitud de anulación enviada", "success");
				$scope.closeThisDialog("Yay!");
			});
		}
}])

.controller("FuenteFinanciamientoController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "urls", "FuenteFinanciamiento", "Pagination", "SweetAlert",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, urls, FuenteFinanciamiento, Pagination, SweetAlert)
{
	$rootScope.currentModule 		= "recursoshumanos";
	$rootScope.currentSubModule		= "fuentesfinanciamiento";
	$scope.new_fuentefinanciamiento	= {}
	$scope.pagination 				= Pagination.getNew(10);

	if( $scope.article )
	{
		var id = $routeParams.id
		
		FuenteFinanciamiento.get({id:id}, function(data)
		{
			$scope.fuentefinanciamiento = data.record;
		});
	}

	if( $scope.listview )
	{
		FuenteFinanciamiento.get(function(data)
		{
			$scope.fuentesfinanciamiento = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		$scope.submit = function()
		{
			if( $scope.newform )
			{
				FuenteFinanciamiento.save( $scope.new_fuentefinanciamiento, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Fuente de Financiamiento registrada", "success");
					$location.path( "recursoshumanos/fuentesfinanciamiento" );
				});
			}
		}
	}
}])

.controller("HeaderController", ["$rootScope", "$scope", "$location", "$window", "Auth", "Notificacion", "Socket",
function($rootScope, $scope, $location, $window, Auth, Notificacion, Socket)
{
	$scope.total_notificaciones = 0;
	$scope.current_date 		= new Date();

	Notificacion.get(function(data)
	{
		$scope.notificaciones 		= data.records;
	});

	Auth.getAuthenticatedUser(function(data)
	{
		$scope.usuario 	= data.record;
		$rootScope.me 	= data.record;

		Socket.emit("user:join", data.record.id_unidad);
	});

	$scope.openManual = function()
		{
			$window.open("manual_tracking.pdf")
		}

	$scope.view = function(notificacion, api, idapi)
		{
			if( notificacion.visto == 0 )
			{
				notificacion.visto = 1;
				$scope.total_notificaciones--;
			}

			Notificacion.update( notificacion, function(data)
			{
				$location.path( api + "/" + idapi );
			});
		}

	Socket.on("send:notification", function( notificacion )
	{
		console.log( notificacion );
		//alert("Notificación recibida, revisa tu bandeja.");
		Notificacion.get(function(data)
		{
			$scope.notificaciones 		= data.records;
		});
	});
}])

.controller("HomeController", ["$rootScope", "$scope", "Documento",
function($rootScope, $scope, Documento)
{
	$rootScope.currentModule = "home";
	$scope.current_date = new Date();

	Documento.get(function(data)
	{
		$scope.documentos 			= data.records;
		$scope.documentosDirector 	= data.director;
	});
}])

.controller("LogoutController", ["$rootScope", "$scope", "$window", "$localStorage", "Auth",
function($rootScope, $scope, $window, $localStorage, Auth)
{
	Auth.logout(function()
	{
		$window.location.href = "login";
	});
}])

/*
=============================================================
 !ALL OF "OFICIOS" IT'S DEPRECATED, NOW ARE "DOCUMENTOS" ONY
=============================================================
.controller("OficioController", ["$rootScope", "$scope", "$location", "$routeParams", "Asunto", "Oficio", "OficioAsignacion", "Estado", "Institucion", "Usuario",
function($rootScope, $scope, $location, $routeParams, Asunto, Oficio, OficioAsignacion, Estado, Institucion, Usuario)
{
	$rootScope.currentModule 	= "oficios";
	$scope.current_date 		= new Date();

	$scope.oficio =
	{
		dias_respuesta: 0
	};

	if( $scope.article )
	{
		var id = $routeParams.id
		$scope.reAsignacion =
		{
			id_documento: id
		};

		Oficio.get({id:id}, function(data)
		{
			$scope.oficio = data.record;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	if( $scope.listview )
	{
		$scope.oficioFiltro =
		{
			id_estado: 1
		};

		Estado.get(function(data)
		{
			$scope.estados = data.records;
		});

		Oficio.get(function(data)
		{
			$scope.oficios = data.records;
		});
	}

	if( $scope.newform )
	{
		Asunto.get(function(data)
		{
			$scope.asuntos = data.records;
		});

		Institucion.get(function(data)
		{
			$scope.instituciones = data.records;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	$scope.reply = function(asignacion)
		{
			OficioAsignacion.update( asignacion, function(data)
			{
				asignacion.id_estado		= data.record.id_estado;
				asignacion.fecha_cierre		= data.record.fecha_cierre;
				asignacion.fecha_recibido	= data.record.fecha_recibido;
				asignacion.mensaje_cierre	= data.record.mensaje_cierre;
				asignacion.mensaje_recibido	= data.record.mensaje_recibido;

				SweetAlert.swal("Operación exitosa", "Haz respondido a este oficio", "success");
			});
		}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Oficio.save( $scope.oficio, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Oficio creado", "success");
					$location.path( "oficios" );
				});
			}
		}

	$scope.transfer = function(reAsignacion)
		{
			console.log( reAsignacion );
			OficioAsignacion.save( reAsignacion, function(data)
			{
				SweetAlert.swal("Operación exitosa", "El Oficio ha sido transferido", "success");
			});
		}
}])
*/

.controller("PagoController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "urls", "PagoCheque", "Pagination", "Puesto", "Renglon", "SweetAlert", "TempPersona", "Unidad",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, urls, PagoCheque, Pagination, Puesto, Renglon, SweetAlert, TempPersona, Unidad)
{
	$rootScope.currentModule 	= "pagos";
	$scope.new_pagocheque		= {}
	$scope.pagination 			= Pagination.getNew(10);

	if( $scope.article )
	{
		var id = $routeParams.id
		
		PagoCheque.get({id:id}, function(data)
		{
			$scope.pagocheque = data.record;
		});
	}

	if( $scope.listview )
	{
		PagoCheque.get(function(data)
		{
			$scope.pagos = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		Puesto.get(function(data)
		{
			$scope.puestos = data.records;
		});

		Renglon.get(function(data)
		{
			$scope.renglones = data.records;
		});

		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});

		$scope.submit = function()
		{
			if( $scope.newform )
			{
				console.log( $scope.new_pagocheque );
				PagoCheque.save( $scope.new_pagocheque, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Pago registrado", "success");
					$location.path( "pagos" );
				});
			}
		}
	}

	/*
	$scope.buscarPersona = function()
	{
		$scope.busqueda = 
		{
			"nombre": $scope.new_pagocheque.nombre_1,
			"numero_dpi": $scope.new_pagocheque.numero_dpi
		};

		TempPersona.get($scope.busqueda, function(data)
		{
			console.log( "XXX" );
			console.log( data );
			$scope.procedimiento = data.record;
		});
	}
	*/

	$scope.actualizarUnidades = function()
	{
		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});
	}

	$scope.limpiarFotoCheque = function()
	{
		$scope.new_pagocheque.src_cheque = "";
	}

	$scope.limpiarFotoPersona = function()
	{
		$scope.new_pagocheque.src_foto = "";
	}

	$scope.generateReportPagoCheque = function()
		{
			$window.open( urls.BASE_REPORT + "/pagocheque?id=" + $scope.pagocheque.id );
		}
}])

.controller("PartidaPresupuestariaController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "urls", "FuenteFinanciamiento", "Pagination", "PartidaPresupuestaria", "Renglon", "SweetAlert",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, urls, FuenteFinanciamiento, Pagination, PartidaPresupuestaria, Renglon, SweetAlert)
{
	$rootScope.currentModule 			= "recursoshumanos";
	$rootScope.currentSubModule			= "partidaspresupuestarias";
	$scope.new_partidapresupuestaria	= {}
	$scope.pagination 					= Pagination.getNew(10);

	$scope.actualizarFuentesFinanciamiento = function()
		{
			FuenteFinanciamiento.get(function(data)
			{
				$scope.fuentesfinanciamiento = data.records;
			});
		}

	$scope.actualizarRenglones = function()
		{
			Renglon.get(function(data)
			{
				$scope.renglones = data.records;
			});
		}

	if( $scope.article )
	{
		var id = $routeParams.id
		
		PartidaPresupuestaria.get({id:id}, function(data)
		{
			$scope.partidapresupuestaria = data.record;
		});
	}

	if( $scope.listview )
	{
		PartidaPresupuestaria.get(function(data)
		{
			$scope.partidaspresupuestarias = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		$scope.actualizarFuentesFinanciamiento();
		$scope.actualizarRenglones();

		$scope.submit = function()
		{
			if( $scope.newform )
			{
				PartidaPresupuestaria.save( $scope.new_partidapresupuestaria, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Fuente de Financiamiento registrada", "success");
					$location.path( "recursoshumanos/fuentesfinanciamiento" );
				});
			}
		}
	}
}])

.controller("ProcedimientoController", ["$rootScope", "$scope", "$filter", "$localStorage", "$location", "$window", "$routeParams", "urls", "Procedimiento", "ProcedimientoActividad", "Pagination", "SweetAlert", "Unidad",
function($rootScope, $scope, $filter, $localStorage, $location, $window, $routeParams, urls, Procedimiento, ProcedimientoActividad, Pagination, SweetAlert, Unidad)
{
	$rootScope.currentModule 	= "procedimientos";
	$scope.new_procedimiento 	= {}
	$scope.pagination 			= Pagination.getNew(10);
	
	if( $scope.article )
	{
		var id = $routeParams.id
		
		Procedimiento.get({id:id}, function(data)
		{
			console.log( "XXX" );
			console.log( data );
			$scope.procedimiento = data.record;
		});
	}

	if( $scope.listview )
	{
		Procedimiento.get(function(data)
		{
			$scope.procedimientos = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		$scope.new_actividad					= {};
		$scope.new_norma						= { descripcion: "" };
		$scope.new_objetivo						= { descripcion: "" };
		$scope.new_procedimiento				= 
		{
			actividades: [],
			list_normas: [],
			list_objetivos: [],
			normas: "",
			objetivos: ""
		};

		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		$scope.new_actividad					= {};
		$scope.new_norma						= { descripcion: "" };
		$scope.new_objetivo						= { descripcion: "" };

		Procedimiento.get({id:id}, function(data)
		{
			$scope.new_procedimiento = data.record;
		});

		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});
	}

	$scope.addActividad = function()
		{
			if( $scope.new_actividad.unidad != "" || $scope.new_actividad.puesto != "" || $scope.new_actividad.actividad != "" )
			{
				$scope.new_procedimiento.actividades.push( $scope.new_actividad );
				$scope.new_actividad = {};
			}
		}

	$scope.addNorma = function()
		{
			if( $scope.new_norma.descripcion != "" )
			{
				$scope.new_procedimiento.list_normas.push( $scope.new_norma );
				$scope.new_norma = { descripcion: "" };
			}
		}

	$scope.addObjetivo = function()
		{
			if( $scope.new_objetivo.descripcion != "" )
			{
				$scope.new_procedimiento.list_objetivos.push( $scope.new_objetivo );
				$scope.new_objetivo = { descripcion: "" };
			}
		}

	$scope.generateReportProcedimiento = function()
		{
			$window.open( urls.BASE_REPORT + "/procedimiento?id=" + $scope.procedimiento.id );
		}

	$scope.removeNorma = function( index )
		{
			if( index !== undefined )
			{
				$scope.new_procedimiento.list_normas.splice( index, 1 );
			}
		}

	$scope.removeObjetivo = function( index )
		{
			if( index !== undefined )
			{
				$scope.new_procedimiento.list_objetivos.splice( index, 1 );
			}
		}

	$scope.submit = function()
		{
			// Stringify! because why not...
			$scope.new_procedimiento.json_actividades 	= JSON.stringify( $scope.new_procedimiento.actividades );
			
			for( x in $scope.new_procedimiento.list_normas )
			{
				$scope.new_procedimiento.normas += "<li>" + $scope.new_procedimiento.list_normas[x].descripcion + "</li>";
			}

			for( x in $scope.new_procedimiento.list_objetivos )
			{
				$scope.new_procedimiento.objetivos += "<li>" + $scope.new_procedimiento.list_objetivos[x].descripcion + "</li>";
			}

			//$scope.new_procedimiento.normas 	= "<ul>" + $scope.new_procedimiento.normas + "</ul>";
			//$scope.new_procedimiento.objetivos 	= "<ol>" + $scope.new_procedimiento.objetivos + "</ol>";
			
			if( $scope.newform )
			{
				Procedimiento.save( $scope.new_procedimiento, function(data)
				{
					console.log( data );
					SweetAlert.swal("Operación exitosa", "Procedimiento creado", "success");
					$location.path( "procedimientos" );
				});
			}
			else if( $scope.editform )
			{
				Procedimiento.update( $scope.new_procedimiento, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Procedimiento modificado", "success");
					$location.path( "administracion" );
				});
			}
		}
}])

.controller("RecursosHumanosController", ["$rootScope", "$scope", "$controller", "$filter", "$location", "$routeParams", "Departamento", "EstadoCivil", "Municipio", "NivelAcademico", "Unidad", "Pais", "Password", "Profesion", "Puesto", "SweetAlert", "Usuario",
function($rootScope, $scope, $controller, $filter, $location, $routeParams, Departamento, EstadoCivil, Municipio, NivelAcademico, Unidad, Pais, Password, Profesion, Puesto, SweetAlert, Usuario)
{
	angular.extend(this, $controller("UsuarioController", {$scope: $scope}));
	$scope.currentModule 	= "recursoshumanos";
	$scope.currentSubModule = "personal";
	$scope.rrhh 			= true;

	if( $scope.newform )
	{
		$scope.emergencia 						= {};
		$scope.hijo 							= {};
		$scope.informacion_academica 			= {};
		$scope.padre 							= {};
		$scope.usuario 							= {};
		$scope.usuario.hijos 					= [];
		$scope.usuario.informacion_academica 	= [];
		$scope.usuario.informacion_emergencia 	= [];
		$scope.usuario.padres 					= [];
		$scope.usuario.rrhh 					= 1;

		Departamento.get(function(data)
		{
			$scope.departamentos = data.records;
		});

		EstadoCivil.get(function(data)
		{
			$scope.estadocivil = data.records;
		});

		Municipio.get(function(data)
		{
			$scope.municipios = data.records;
		});

		NivelAcademico.get(function(data)
		{
			$scope.nivelacademico = data.records;
		});

		Pais.get(function(data)
		{
			$scope.paises = data.records;
		});
	}

	$scope.addHijo = function()
		{
			if( $scope.hijo.nombre != "" && $scope.hijo.fecha_nacimienito != "" && $scope.hijo.sexo != "" )
			{
				var formatFecha = $filter("date")($scope.hijo.fecha_nacimienito, "yyyy-MM-dd");
				$scope.hijo.fecha_nacimienito = formatFecha;

				$scope.usuario.hijos.push( $scope.hijo );
				$scope.hijo = {};
			}
		}

	$scope.addInformacionAcademica = function()
		{
			if( $scope.informacion_academica.nivel != "" && $scope.informacion_academica.establecimiento != "" 
				&& $scope.informacion_academica.fecha != "" && $scope.informacion_academica.titulo != "" )
			{
				var formatFecha = $filter("date")($scope.informacion_academica.fecha, "yyyy-MM-dd");
				$scope.informacion_academica.fecha = formatFecha;

				$scope.usuario.informacion_academica.push( $scope.informacion_academica );
				$scope.informacion_academica = {};
			}
		}

	$scope.addInformacionEmergencia = function()
		{
			if( $scope.emergencia.nombre != "" && $scope.emergencia.direccion != ""
				&& $scope.emergencia.telefono != "" && $scope.emergencia.parentesco != "" )
			{
				$scope.usuario.informacion_emergencia.push( $scope.emergencia );
				$scope.emergencia = {};
			}
		}

	$scope.addPadre = function()
		{
			if( $scope.padre.sexo != "" && $scope.padre.nombre != "" && $scope.padre.id_estado_civil != ""
				&& $scope.padre.direccion != "" && $scope.padre.telefono != "" )
			{
				$scope.usuario.padres.push( $scope.padre );
				$scope.padre = {};
			}
		}

	$scope.submit = function()
		{
			var formatFechaNacimiento 			= $filter("date")($scope.usuario.fecha_nacimiento, "yyyy-MM-dd");
			$scope.usuario.fecha_nacimiento 	= formatFechaNacimiento;

			/* ===== DEPRECATED =====
			//First create dot notation values for arrays objects (hijos, padres, emergencia, colegios)
			var dot_informacion_academica 	= $filter("dotnotation")($scope.usuario.informacion_academica, "informacion_academica");
			var dot_informacion_emergencia 	= $filter("dotnotation")($scope.usuario.informacion_emergencia, "informacion_emergencia");
			var dot_hijos 					= $filter("dotnotation")($scope.usuario.hijos, "hijos");
			var dot_padre 					= $filter("dotnotation")($scope.usuario.padres, "padres");
			// Now merge (extend) the objects to send data
			angular.extend( $scope.usuario, dot_informacion_academica, dot_informacion_emergencia, dot_hijos, dot_padre );
			========================= */

			// Stringify! because why not...
			$scope.usuario.json_informacion_academica 	= JSON.stringify( $scope.usuario.informacion_academica );
			$scope.usuario.json_informacion_emergencia 	= JSON.stringify( $scope.usuario.informacion_emergencia );
			$scope.usuario.json_hijos 					= JSON.stringify( $scope.usuario.hijos );
			$scope.usuario.json_padres 					= JSON.stringify( $scope.usuario.padres );

			if( $scope.newform )
			{
				Usuario.save( $scope.usuario, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Persona ingresada", "success");
					//$location.path( "recursoshumanos" );
				});
			}
		}
}])

.controller("ReporteController", ["$rootScope", "$scope", "$window", "$httpParamSerializer", "urls", "Unidad", "Reporte", "Usuario",
function($rootScope, $scope, $window, $httpParamSerializer, urls, Unidad, Reporte, Usuario)
{
	$rootScope.currentModule = "reportes";
	$scope.current_date = new Date();

	$scope.reporte_documento 	= {};
	$scope.reporte_expediente 	= {};
	$scope.reporte_firmae 		= {};
	$scope.reporte_operaciones 	= {};

	Unidad.get(function(data)
	{
		$scope.unidades = data.records;
	});

	Usuario.get(function(data)
	{
		$scope.usuarios = data.records;
	});

	$scope.generateReportDocumento = function()
		{
			var params = $httpParamSerializer( $scope.reporte_documento );
			$window.open( urls.BASE_REPORT + "/documento?" + params );
		}

	$scope.generateReportExpedientes = function()
		{
			var params = $httpParamSerializer( $scope.reporte_expediente );
			$window.open( urls.BASE_REPORT + "/expedientes?" + params );
		}

	$scope.generateReportFirmaElectronica = function()
		{
			var params = $httpParamSerializer( $scope.reporte_firmae );
			$window.open( urls.BASE_REPORT + "/firmaelectronica?" + params );
		}

	$scope.generateReportOperaciones = function()
		{
			var params = $httpParamSerializer( $scope.reporte_operaciones );
			$window.open( urls.BASE_REPORT + "/operaciones?" + params );
		}
}])

.controller("UsuarioController", ["$rootScope", "$scope", "$location", "$routeParams", "Unidad", "Pagination", "Password", "Profesion", "Puesto", "SweetAlert", "Usuario",
function($rootScope, $scope, $location, $routeParams, Unidad, Pagination, Password, Profesion, Puesto, SweetAlert, Usuario)
{
	$rootScope.currentModule = "usuarios";

	$scope.pagination 	= Pagination.getNew(10);
	$scope.usuario 		= {};

	if( $scope.article )
	{
		var id = $routeParams.id
		
		Usuario.get({id:id}, function(data)
		{
			$scope.usuario = data.record;
		});
	}

	if( $scope.listview )
	{
		$scope.usuarioFiltro = {};

		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});

		Profesion.get(function(data)
		{
			$scope.profesiones = data.records;
		});

		Puesto.get(function(data)
		{
			$scope.puestos = data.records;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
			$scope.pagination.numPages = Math.ceil(data.records.length/$scope.pagination.perPage);
		});
	}

	if( $scope.newform )
	{
		Unidad.get(function(data)
		{
			$scope.unidades = data.records;
		});

		Profesion.get(function(data)
		{
			$scope.profesiones = data.records;
		});

		Puesto.get(function(data)
		{
			$scope.puestos = data.records;
		});
	}

	$scope.changePassword = function()
		{
			if( $scope.article )
			{
				Password.save( $scope.usuario, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Acabas de cambiar tu contraseña", "success");
					$location.path( "home" );
				});
			}
		}

	$scope.generateUsername = function()
		{
			if( $scope.newform )
			{
				var temp_username = "";

				temp_username += $scope.usuario.nombre_1.trim().substr(0,1);

				if( $scope.usuario.apellido_1 !== undefined )
				{
					temp_username += $scope.usuario.apellido_1.trim();
				}

				if( $scope.usuario.apellido_2 !== undefined )
				{
					temp_username += $scope.usuario.apellido_2.trim().substr(0,1);
				}

				$scope.usuario.usuario = temp_username.toLowerCase();
			}
		}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				console.log( $scope.usuario );
				Usuario.save( $scope.usuario, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Usuario creado", "success");
					$location.path( "usuarios" );
				});
			}
		}
}])

.directive("apiSrc", ["$http", "$localStorage", function ($http, $localStorage)
{
	var directive =
	{
		link: link,
		restrict: "A"
	};
	return directive;

	function link(scope, element, attrs)
	{
		var requestConfig =
		{
			method: "GET",
			url: attrs.apiSrc,
			responseType: "arraybuffer",
			cache: "true",
			headers:
			{
				"Authorization": "Bearer " + $localStorage.token
			}
		};

		$http(requestConfig).success(function(data)
		{
			var arr = new Uint8Array(data);

			var raw = "";
			var i, j, subArray, chunk = 5000;
			for (i = 0, j = arr.length; i < j; i += chunk)
			{
				subArray = arr.subarray(i, i + chunk);
				raw += String.fromCharCode.apply(null, subArray);
			}

			var b64 = btoa(raw);
			attrs.$set("src", "data:image/jpeg;base64," + b64);
		});
	}
}])

.directive("stringToNumber", function()
{
	return {
		require: "ngModel",
		link: 
			function(scope, element, attrs, ngModel)
			{
				ngModel.$parsers.push(
					function(value)
					{
						return "" + value;
					}
				);
			
				ngModel.$formatters.push(
					function(value)
					{
						return parseFloat(value, 10);
					}
				);
			}
	}
})

.factory("Auth", ["$http", "$localStorage", "urls", function ($http, $localStorage, urls)
{
	function urlBase64Decode(str)
	{
		var output = str.replace("-", "+").replace("_", "/");
		switch (output.length % 4)
		{
			case 0:
				break;
			case 2:
				output += "==";
				break;
			case 3:
				output += "=";
				break;
			default:
				throw "Illegal base64url string!";
		}

		return window.atob(output);
	}

	function getClaimsFromToken()
	{
		var token = $localStorage.token;
		var user = {};
		if (typeof token !== "undefined")
		{
			var encoded = token.split(".")[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}

		return user;
	}

	var tokenClaims = getClaimsFromToken();

	return {
		signup: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		signin: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		logout: function (success)
		{
			tokenClaims = {};
			delete $localStorage.token;
			success();
		},
		getTokenClaims: function ()
		{
			return tokenClaims;
		},
		getAuthenticatedUser: function(success)
		{
			$http.get(urls.BASE_API + "/authenticate").success(success);
		}
	};
}])

.factory("Asunto", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/asunto");
}])

.factory("BoletaAutorizacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/boletaautorizacion/:id");
}])

.factory("Departamento", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/departamento");
}])

.factory("Documento", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/documento/:id");
}])

.factory("DocumentoAnulacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/documento/anulacion");
}])

.factory("DocumentoAsignacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/documento/asignacion/:id", 
						{ 
							id: "@id", 
							compartido: "@compartido",
							mensaje_asignacion: "@mensaje_asignacion", 
							mensaje_cierre: "@mensaje_cierre", 
							mensaje_recibido: "@mensaje_recibido",
						},
						{
							"update": { method: "PUT" }
						});
}])

.factory("DocumentoObservacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/documento/observacion");
}])

.factory("Estado", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/estado");
}])

.factory("EstadoCivil", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/estadocivil");
}])

.factory("FuenteFinanciamiento", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/fuentefinanciamiento/:id");
}])

.factory("Institucion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/institucion");
}])

.factory("Municipio", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/municipio");
}])

.factory("NivelAcademico", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/nivelacademico");
}])

.factory("Notificacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/notificacion/:id", { id: "@id", visto: "@visto" },
						{
							"update": { method: "PUT" }
						});
}])

/*
=============================================================
 !ALL OF "OFICIOS" IT'S DEPRECATED, NOW ARE "DOCUMENTOS" ONY
=============================================================
.factory("Oficio", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/oficio/:id", 
						{ 
							id: "@id"
						},
						{
							"update": { method: "PUT" }
						});
}])

.factory("OficioAsignacion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/oficio/asignacion/:id", { id: "@id", mensaje_cierre: "@mensaje_cierre", mensaje_recibido: "@mensaje_recibido" },
						{
							"update": { method: "PUT" }
						});
}])
*/

.factory("PagoCheque", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/pagocheque/:id");
}])

.factory("Pais", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/pais");
}])

.factory("PartidaPresupuestaria", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/partidapresupuestaria/:id");
}])

.factory("Password", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/password");
}])

.factory("Procedimiento", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/procedimiento/:id",
						{ 
							"id": "@id",
							"definicion": "@definicion",
							"json_actividades": "@json_actividades",
							"normas": "@normas",
							"objetivos": "@objetivos",
							"titulo": "@titulo",
							"unidad": "@unidad"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("ProcedimientoActividad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/procedimiento/actividad");
}])

.factory("Profesion", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/profesion");
}])

.factory("Puesto", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/puesto/:id", 
						{ 
							"id": "@id",
							"director": "@director",
							"monitor": "@monitor",
							"nombre": "@nombre"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Renglon", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/renglon");
}])

.factory("Reporte", ["$resource", "urls", function($http, urls)
{
	return {
		documento: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/documento", data).success(success).error(error)
		},
		expedientes: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/expedientes", data).success(success).error(error)
		},
		operaciones: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/operaciones", data).success(success).error(error)
		}
	};
}])

.factory("Sequence", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/sequence");
}])

.factory("Socket", ["socketFactory", function(socketFactory)
{
	var myIoSocket = io.connect("http://infom.gob.gt:8080");

	Socket = socketFactory({ ioSocket: myIoSocket });
	return Socket;
}])

.factory("SubSequence", ["$http", "urls", function ($http, urls)
{
	return {
		save: function(data, success)
		{
			$http.post(urls.BASE_API + "/sequence/sub", data).success(success);
		}
	};
}])

.factory("TempPersona", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/temppersona", { nombre: "@nombre", numero_dpi: "@numero_dpi" });
}])

.factory("TipoServicio", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/tiposervicio/:id");
}])

.factory("Unidad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/unidad/:id", 
						{ 
							"id": "@id",
							"id_director": "@id_director",
							"id_gerente": "@id_gerente",
							"id_institucion": "@id_institucion",
							"id_supervisor": "@id_supervisor",
							"nombre": "@nombre",
							"siglas": "@siglas"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Usuario", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/usuario/:id", { id: "@id" });
}])

.filter("abs", function ()
{
	return function(val) { return Math.abs(val); }
})

.filter("dotnotation", function ()
{
	return function(arr, property) { 
		var obj 		= {};

		for( item in arr ) // Loop the objects of the array
		{
			for( key in arr[item] ) // Loop the keys of the object
			{
				var num = parseInt(item) + 1;
				obj[ property + "." + num + "." + key ] = arr[item][key];
			}
		}

		return obj;
	}
})

.filter("go", ["$location", function ($location)
{
	return function(val) { $location.path( val ); }
}])

.filter("pad", function ()
{
	return function( num, size ) {
		var s = num + "";
		while (s.length < size) s = "0" + s;
		return s;
	};
})

.run(["$location", "$rootScope", "urls", function($location, $rootScope, urls)
{
	$rootScope.$on("$routeChangeSuccess", function (event, current, previous)
	{
		if( current.$$route !== undefined )
		{
			$rootScope.article 	= current.$$route.article;
			$rootScope.editform = current.$$route.editform;
			$rootScope.listview = current.$$route.listview;
			$rootScope.newform 	= current.$$route.newform;
			$rootScope.title 	= current.$$route.title + urls.APP_TITLE;
			$rootScope.showNotificacionDropdown = false;
			$rootScope.showUserDropdown 		= false;
		}
	});
}]);