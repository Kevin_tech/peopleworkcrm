<section class="col_full content-article">
	<h2>{{procedimiento.titulo}}</h2>

	<article>
		<label class="seq">
			<button class="print" ng-click="generateReportProcedimiento()">
				<span class="icon-print"></span>
				Obtener Reporte
			</button>
			<button class="print" ng-click="'procedimientos/{{procedimiento.id}}/edit' | go">
				<span class="icon-edit"></span>
				Modificar Procedimiento
			</button>
		</label>
		<p>
			<b>Unidad Administrativa</b><br/>
			{{procedimiento.unidad}}
		</p>
		<p>
			<b>Objetivos del Procedimiento</b><br/>
			<ul ng-bind-html="procedimiento.objetivos"></ul>
		</p>
		<p>
			<b>Normas del Procedimiento</b><br/>
			<ol ng-bind-html="procedimiento.normas"></ol>
		</p>
		<div class="clear"></div>
		<div class="details">
			<label class="author">
				<span class="icon-edit"></span>
				<strong>Creado por</strong>
				{{procedimiento.usuario.apellido_1}} {{procedimiento.usuario.apellido_2}}, {{procedimiento.usuario.nombre_1}} {{procedimiento.usuario.nombre_2}}
			</label>
			<label class="date">
				<span class="icon-today"></span>
				{{procedimiento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</label>
			<label class="date">
				<span class="icon-access_time"></span>
				{{procedimiento.dias_respuesta}} día(s) para responder
			</label>
		</div>
	</article>

	<div class="messages content-block">
		<!-- Asignaciones area -->
		<div class="col_full">
			<h3>Actividades</h3>
			<table class="table-view">
				<thead>
					<tr>
						<th>Paso No.</th>
						<th>Unidad Responsable</th>
						<th>Puesto Responsable</th>
						<th>Actividad</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="actividad in procedimiento.actividad">
						<td>
							<span class="index">{{$index+1}}</span>
						</td>
						<td>{{actividad.unidad}}</td>
						<td>{{actividad.puesto}}</td>
						<td>{{actividad.actividad}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>