<section class="col_full content-article">
	<h2>
		{{boletaautorizacion.apellido_1}} {{boletaautorizacion.apellido_2}} {{boletaautorizacion.apellido_3}},
		{{boletaautorizacion.nombre_1}} {{boletaautorizacion.nombre_2}} {{boletaautorizacion.nombre_3}}
	</h2>

	<article>
		<label class="seq">
			DPI: {{boletaautorizacion.numero_dpi}}
			<button class="print" ng-click="generateReportBoletaAutorizacion()">
				<span class="icon-print"></span>
				Obtener Reporte
			</button>
		</label>
		<p>
			<b>Renglón</b><br/>
			{{boletaautorizacion.renglon.nombre}}
		</p>
		<p>
			<b>Unidad</b><br/>
			{{boletaautorizacion.unidad.nombre}}
		</p>
		<p>
			<b>Tipo de Servicio</b><br/>
			{{boletaautorizacion.tipo_servicio.nombre}}
		</p>
		<p>
			<b>Partida Presupuestaria</b><br/>
			{{boletaautorizacion.partida_presupuestaria.actividad}} -
			{{boletaautorizacion.partida_presupuestaria.ejercicio_fiscal}} -
			{{boletaautorizacion.partida_presupuestaria.entidad}} -
			{{boletaautorizacion.partida_presupuestaria.obra}} -
			{{boletaautorizacion.partida_presupuestaria.programa}} -
			{{boletaautorizacion.partida_presupuestaria.proyecto}} -
			{{boletaautorizacion.partida_presupuestaria.sub_programa}} -
			{{boletaautorizacion.partida_presupuestaria.ubicacion}} -
			{{boletaautorizacion.partida_presupuestaria.unidad_desconcentrada}} -
			{{boletaautorizacion.partida_presupuestaria.unidad_ejecutora}}
		</p>
		<p>
			<b>Puesto que ocupa</b><br/>
			{{boletaautorizacion.puesto}}
		</p>
		<div class="clear"></div>
		<div class="details">
			<label class="date">
				<span class="icon-today"></span>
				{{boletaautorizacion.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</label>
		</div>
	</article>

	<div class="messages content-block">
		<!-- Asignaciones area -->
		<div class="col_full">
			<h3>Datos sobre el pago</h3>
			<p>
				<b>Pago Inicial</b><br/>
				Q. {{boletaautorizacion.pago_1}}
			</p>
			<p>
				<b>Cantidad de Pagos</b><br/>
				{{boletaautorizacion.pago_cantidad}}
			</p>
			<p>
				<b>Valor de los Pagos</b><br/>
				Q. {{boletaautorizacion.pago_valor}}
			</p>
		</div>
	</div>
</section>