<section class="col_full content-article">
	<h2>{{documento.asunto}}</h2>

	<article>
		<!--<img ng-if="documento.src_file.length>0" class="image" api-src="/api/documento/{{documento.id}}/imagen">-->
		<label class="seq">
			Correlativo: {{documento.correlativo}}
			<button ng-if="documento.src_file.length>0" class="print" ng-click="openFile()">
				<span class="icon-file_download"></span>
				Descargar adjunto
			</button>
			<button class="print" ng-click="generateReportDocumento()">
				<span class="icon-print"></span>
				Obtener Reporte
			</button>
		</label>
		{{documento.contenido}}
		<div class="clear"></div>
		<div class="details">
			<label class="author">
				<span class="icon-edit"></span>
				<strong>Creado por</strong>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
			</label>
			<label class="date">
				<span class="icon-today"></span>
				{{documento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</label>
			<label class="date">
				<span class="icon-access_time"></span>
				{{documento.dias_respuesta}} día(s) para responder
			</label>
		</div>
	</article>

	<div class="messages listview-blocks">
		<!-- Asignaciones area -->
		<div class="col_2_3">
			<h3>Asignación</h3>
			<div class="item" ng-repeat="asignacion in documento.asignacion">
				<span class="date">{{asignacion.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
				<label class="user">
					<span class="icon-person"></span>
					{{asignacion.usuario_asignado.apellido_1}} {{asignacion.usuario_asignado.apellido_2}},
					{{asignacion.usuario_asignado.nombre_1}} {{asignacion.usuario_asignado.nombre_2}}
				</label>
				<p class="txt" ng-if="asignacion.mensaje_asignacion.length>0">
					<strong>Mensaje de traslado
					({{asignacion.usuario_asigno.apellido_1}} {{asignacion.usuario_asigno.apellido_2}},
					{{asignacion.usuario_asigno.nombre_1}} {{asignacion.usuario_asigno.nombre_2}}):</strong>
					{{asignacion.mensaje_asignacion}}
				</p>
				<p class="txt" ng-if="asignacion.fecha_recibido != '0000-00-00 00:00:00'">
					<strong>Mensaje de recibido:</strong>
					{{asignacion.mensaje_recibido}}
				</p>
				<p class="txt" ng-if="asignacion.fecha_cierre != '0000-00-00 00:00:00'">
					<strong>Mensaje de finalizado:</strong>
					{{asignacion.mensaje_cierre}}
				</p>
				<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_recibido == '0000-00-00 00:00:00'">
					<form class="form-inline" ng-submit="reply(asignacion)">
						<div class="input-field">
							<input type="text" ng-model="asignacion.mensaje_recibido" placeholder="Envía un mensaje de recibido..." required="" />
							<span class="icon-chat_bubble_outline"></span>
							<button class="submit">
								Responder
								<span class="icon-reply"></span>
							</button>
						</div>
					</form>
				</div>
				<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_cierre == '0000-00-00 00:00:00' && asignacion.fecha_recibido != '0000-00-00 00:00:00'">
					<form class="form-inline" ng-submit="reply(asignacion)">
						<div class="input-field">
							<input type="text" ng-model="asignacion.mensaje_cierre" placeholder="Envía un mensaje y marca como finalizado..." required="" />
							<span class="icon-chat_bubble_outline"></span>
							<button class="submit">
								Finalizar
								<span class="icon-send"></span>
							</button>
						</div>
					</form>
				</div>
				<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_cierre == '0000-00-00 00:00:00'">
					<form class="form-inline" ng-submit="transfer(reAsignacion)">
						<div class="input-choose">
							<!--<input type="text" ng-change="reAsignacion.id_usuario_asignado = (usuarios | filter:usuarioReplyFiltro | limitTo:1)[0].id"
									ng-model="usuarioReplyFiltro.$" placeholder="Buscar usuario por nombre y/o departamento..." />-->
							<input type="text" ng-model="reAsignacion.mensaje_asignacion" placeholder="Envía un mensaje al usuario..." required="" />
							<select ng-model="reAsignacion.id_usuario_asignado" >
								<option ng-repeat="usuario in usuarios | filter:usuarioReplyFiltro" 
										ng-selected="usuario.id == reAsignacion.id_usuario_asignado" value="{{usuario.id}}">
									{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}
								</option>
							</select>
							<label>
								<input type="checkbox" ng-model="reAsignacion.compartido" value="1" />
								Tarea compartida
							</label>
							<button class="submit">
								Trasladar
								<span class="icon-assignment_ind"></span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Asignaciones area -->
		<div class="col_1_3">
			<h3>Observaciones (Chat)</h3>
			<div class="chatbox">
				<form class="form-inline" ng-submit="pubObservacion()">
					<div class="input-field">
						<input type="text" ng-model="new_observacion.mensaje" placeholder="Escribe un mensaje..." required="" />
						<button class="submit">
							<span class="icon-send"></span>
						</button>
					</div>
				</form>
				<ul>
					<li ng-repeat="observacion in documento.observacion"
						ng-class="{ me: observacion.id_usuario == me.id }">
						<label class="avatar" ng-if="observacion.id_usuario != me.id">
							<span class="icon-account_circle"></span>
						</label>
						<p class="content">
							<label class="user" 
								ng-if="observacion.id_usuario != me.id">
								{{observacion.usuario.apellido_1}} {{observacion.usuario.apellido_2}},
								{{observacion.usuario.nombre_1}} {{observacion.usuario.nombre_2}}
							</label>
							<label class="user"
								ng-if="observacion.id_usuario == me.id">
								Yo
							</label>
							<label class="msg">
								{{observacion.mensaje}}
							</label>
							<label class="time">
								{{observacion.created_at | amDateFormat:'D/MM/YYYY [a las] hh:mm a'}}
							</label>
						</p>
						<label class="avatar" ng-if="observacion.id_usuario == me.id">
							<span class="icon-account_circle"></span>
						</label>
					</li>
					<li class="not-found" ng-if="documento.observacion.length == 0">
						<label class="avatar"></label>
						<p class="content">
							<span class="icon-chat"></span>
							No hay observaciones aún...
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- Responder con oficio -->
	<div class="keypad">
		<button class="big reply" ng-click="showReplyWith()" 
				ng-if="documento.id_estado>2 && documento.asignacion_activa">
			Responder con oficio
		</button>
		<button class="big remove" 
				ng-click="showSolicitudAnulacion()"
				ng-if="documento.id_estado<3 && ( documento.asignacion_activa || documento.id_usuario_creo == me.id )">
			Anular documento
		</button>
	</div>
</section>

<!-- Crear nuevo oficio de respuesta/traslado manteniendo el correlativo -->
<section class="wrapper" ng-include="'module/documentos'" ng-if="new_documento.id_documento_padre > 0"></section>