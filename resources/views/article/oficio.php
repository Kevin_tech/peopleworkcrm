<section class="col_full content-article">
	<h2>{{oficio.asunto.nombre}}</h2>

	<article>
		<img ng-if="oficio.src_file.length>0" class="image" api-src="/api/oficio/{{oficio.id}}/imagen">
		<label class="seq">
			Correlativo: {{oficio.created_at | amDateFormat:'YYYY'}}-{{oficio.id}}
		</label>
		{{oficio.contenido}}
		<div class="clear"></div>
		<div class="details">
			<label class="author">
				<span class="icon-edit"></span>
				<strong>Creado por</strong>
				{{oficio.usuario_creo.apellido_1}} {{oficio.usuario_creo.apellido_2}}, {{oficio.usuario_creo.nombre_1}} {{oficio.usuario_creo.nombre_2}}
			</label>
			<label class="author">
				<span class="icon-spellcheck"></span>
				<strong>Vo.Bo.</strong>
				{{oficio.usuario_vobo.apellido_1}} {{oficio.usuario_vobo.apellido_2}}, {{oficio.usuario_vobo.nombre_1}} {{oficio.usuario_vobo.nombre_2}}
			</label>
			<label class="date">
				<span class="icon-today"></span>
				{{oficio.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</label>
			<label class="date">
				<span class="icon-access_time"></span>
				{{oficio.dias_respuesta}} día(s) para responder
			</label>
		</div>
	</article>

	<div class="messages listview-blocks">
		<h3>Asignación</h3>
		<div class="item" ng-repeat="asignacion in oficio.asignacion">
			<span class="date">{{asignacion.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
			<label class="user">
				<span class="icon-person"></span>
				{{asignacion.usuario_asignado.apellido_1}} {{asignacion.usuario_asignado.apellido_2}},
				{{asignacion.usuario_asignado.nombre_1}} {{asignacion.usuario_asignado.nombre_2}}
			</label>
			<p class="txt" ng-if="asignacion.fecha_recibido != '0000-00-00 00:00:00'">
				<strong>Mensaje de recibido:</strong>
				{{asignacion.mensaje_recibido}}
			</p>
			<p class="txt" ng-if="asignacion.fecha_cierre != '0000-00-00 00:00:00'">
				<strong>Mensaje de finalizado:</strong>
				{{asignacion.mensaje_cierre}}
			</p>
			<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_recibido == '0000-00-00 00:00:00'">
				<form class="form-inline" ng-submit="reply(asignacion)">
					<div class="input-field">
						<input type="text" ng-model="asignacion.mensaje_recibido" placeholder="Envía un mensaje de recibido..." />
						<span class="icon-chat_bubble_outline"></span>
						<button class="submit">
							Responder
							<span class="icon-reply"></span>
						</button>
					</div>
				</form>
			</div>
			<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_cierre == '0000-00-00 00:00:00' && asignacion.fecha_recibido != '0000-00-00 00:00:00'">
				<form class="form-inline" ng-submit="reply(asignacion)">
					<div class="input-field">
						<input type="text" ng-model="asignacion.mensaje_cierre" placeholder="Envía un mensaje y marca como finalizado..." />
						<span class="icon-chat_bubble_outline"></span>
						<button class="submit">
							Finalizar
							<span class="icon-send"></span>
						</button>
					</div>
				</form>
			</div>
			<div class="reply" ng-if="asignacion.asignacion_activa && asignacion.fecha_cierre == '0000-00-00 00:00:00'">
				<form class="form-inline" ng-submit="transfer(reAsignacion)">
					<div class="input-choose">
						<input type="text" ng-change="reAsignacion.id_usuario_asignado = (usuarios | filter:usuarioFiltro | limitTo:1)[0].id"
								ng-model="usuarioFiltro" placeholder="Buscar usuario por nombre y/o departamento..." />
						<select ng-model="reAsignacion.id_usuario_asignado" >
							<option ng-repeat="usuario in usuarios | filter:usuarioFiltro" 
									ng-selected="usuario.id == reAsignacion.id_usuario_asignado" value="{{usuario.id}}">
								{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}
							</option>
						</select>
						<button class="submit">
							Trasladar
							<span class="icon-assignment_ind"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>