<h2>Ingresa los detalles del nuevo procedimiento</h2>
<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<div class="field-block">
		<label>Unidad Administrativa</label>
		<input type="text" ng-model="new_procedimiento.unidad" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Título del Procedimiento</label>
		<input type="text" ng-model="new_procedimiento.titulo" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Definición del Procedimiento</label>
		<textarea ng-model="new_procedimiento.definicion" rows="8"></textarea>
	</div>

	<h3>Objetivos del Procedimiento</h3>
	<div class="field-block">
		<label>Ingresa los detalles del objetivo</label>
		<textarea ng-model="new_objetivo.descripcion" rows="3"></textarea>
	</div>
	<span ng-click="addObjetivo()" class="btn">Agregar Objetivo</span>
	<ul class="list-detail">
		<li ng-repeat="item in new_procedimiento.list_objetivos">
			<label class="title">{{item.descripcion}}</label>
			<button ng-click="removeObjetivo($index)" class="button"><span class="icon-delete"></span></button>
		</li>
	</ul>

	<h3>Normas del Procedimiento</h3>
	<div class="field-block">
		<label>Ingresa los detalles de la norma</label>
		<textarea ng-model="new_norma.descripcion" rows="3"></textarea>
	</div>
	<span ng-click="addNorma()" class="btn">Agregar Norma</span>
	<ol class="list-detail">
		<li ng-repeat="item in new_procedimiento.list_normas">
			<label class="title">{{item.descripcion}}</label>
			<button ng-click="removeNorma($index)" class="button"><span class="icon-delete"></span></button>
		</li>
	</ol>

	<h3>Listado de Actividades</h3>
	<div class="field-block">
		<label>Unidad Responsable</label>
		<input type="text" ng-model="new_actividad.unidad" />
	</div>
	<div class="field-block">
		<label>Puesto Responsable</label>
		<input type="text" ng-model="new_actividad.puesto" />
	</div>
	<div class="field-block">
		<label>Actividad</label>
		<textarea ng-model="new_actividad.actividad" rows="3"></textarea>
	</div>
	<span ng-click="addActividad()" class="btn">Agregar Actividad</span>
	<table class="table-view">
		<thead>
			<tr>
				<th>Paso No.</th>
				<th>Unidad Responsable</th>
				<th>Puesto Responsable</th>
				<th>Actividad</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="item in new_procedimiento.actividades">
				<td>
					<span class="index">{{$index+1}}</span>
				</td>
				<td>{{item.unidad}}</td>
				<td>{{item.puesto}}</td>
				<td>{{item.actividad}}</td>
			</tr>
		</tbody>
	</table>

	<input type="submit" value="Enviar" name="enviar" />
</form>