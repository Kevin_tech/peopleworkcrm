<h2>Ingresa los detalles del nuevo pago</h2>
<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<h2>Datos Generales</h2>
	<div class="file-upload">
		<label class="title">
			<span class="icon-add_a_photo"></span>
			Foto de la persona
			<span class="icon-refresh" ng-click="limpiarFotoPersona()"></span>
		</label>
		<ng-camera
			type="photo"
			enabled="true"
			width="640"
			height="480"
			countdown="0"
			ng-model="new_pagocheque.src_foto"
			capture-message="Whisky!"
			ng-hide="new_pagocheque.src_foto.length > 0"></ng-camera>
		<img class="preview" src="{{new_pagocheque.src_foto}}" />
	</div>
	<div class="field-block">
		<label>Número de DPI</label>
		<input type="text" ng-model="new_pagocheque.numero_dpi" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Nombres</label>
		<input type="text" ng-model="new_pagocheque.nombre_1" placeholder="Primer nombre" required />
		<input type="text" ng-model="new_pagocheque.nombre_2" placeholder="Segundo nombre"/>
		<input type="text" ng-model="new_pagocheque.nombre_3" placeholder="Tercer nombre" />
	</div>
	<div class="field-block">
		<label>Apellidos</label>
		<input type="text" ng-model="new_pagocheque.apellido_1" placeholder="Primer apellido" required/>
		<input type="text" ng-model="new_pagocheque.apellido_2" placeholder="Segundo apellido" />
		<input type="text" ng-model="new_pagocheque.apellido_3" placeholder="Apellido de casada" />
	</div>
	<div class="field-block">
		<label>Sexo</label>
		<select ng-model="new_pagocheque.sexo" required>
			<option value="1">Masculino</option>
			<option value="2">Femenino</option>
		</select>
	</div>
	<div class="field-block">
		<label>Renglon</label>
		<select ng-model="new_pagocheque.id_renglon" required>
			<option ng-repeat="renglon in renglones" value="{{renglon.id}}">{{renglon.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Unidad <span class="icon-refresh" ng-click="actualizarUnidades()"></span></label>
		<select ng-model="new_pagocheque.id_unidad" required>
			<option ng-repeat="unidad in unidades" value="{{unidad.id}}">{{unidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Puesto que ocupa</label>
		<input type="text" ng-model="new_pagocheque.puesto" placeholder="" required />
	</div>
	
	<h2>Datos del Pago</h2>
	<div class="field-block">
		<label>Número de referencia del cheque</label>
		<input type="text" ng-model="new_pagocheque.numero_cheque" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Monto del cheque</label>
		<input type="text" ng-model="new_pagocheque.monto" placeholder="" required />
	</div>
	<div class="file-upload">
		<label class="title">
			<span class="icon-picture_in_picture"></span>
			Foto del cheque
			<span class="icon-refresh" ng-click="limpiarFotoCheque()"></span>
		</label>
		<ng-camera
			type="photo"
			enabled="true"
			width="640"
			height="480"
			countdown="0"
			ng-model="new_pagocheque.src_cheque"
			capture-message="Whisky!"
			ng-hide="new_pagocheque.src_cheque.length > 0"></ng-camera>
		<img class="preview" src="{{new_pagocheque.src_cheque}}" />
	</div>

	<input type="submit" value="Enviar" name="enviar" />
</form>