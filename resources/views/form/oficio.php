<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<label class="seq">
		Correlativo: {{documento.created_at | amDateFormat:'YYYY'}}-{{documento.id}}
	</label>
	<div class="field-block">
		<label>Asunto</label>
		<select 
			ng-model="oficio.asunto" 
			ng-options="asunto.nombre for asunto in asuntos"
			ng-change="oficio.id_asunto=oficio.asunto.id;oficio.dias_respuesta=oficio.asunto.dias_respuesta"></select>
	</div>
	<div class="field-block">
		<label>Institución</label>
		<select ng-model="oficio.id_institucion">
			<option ng-repeat="institucion in instituciones" value="{{institucion.id}}">{{institucion.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipalidad</label>
		<select ng-model="oficio.id_municipalidad">
			<option ng-repeat="municipalidad in municipalidades" value="{{municipalidad.id}}">{{municipalidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Usuario VoBo</label>
		<select ng-model="oficio.id_usuario_vobo">
			<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}</option>
		</select>
	</div>
	<div class="field-block">
		<label># Documento de Referencia</label>
		<input type="text" ng-model="oficio.documento_referencia" required />
	</div>
	<div class="field-block">
		<label>Contenido</label>
		<textarea ng-model="oficio.contenido" rows="8"></textarea>
	</div>
	<div class="field-block">
		<label>Días para responder</label>
		<input type="number" value="{{oficio.asunto.dias_respuesta}}" string-to-number ng-model="oficio.dias_respuesta" required />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
</form>