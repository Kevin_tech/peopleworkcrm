<form class="content-block form-block" ng-submit="generateReportExpedientes()">
	<div class="field-block">
		<label>Unidad</label>
		<select ng-model="reporte_expediente.id_unidad">
			<option ng-repeat="unidad in unidades"
					ng-if="me.id_unidad==unidad.id" value="{{unidad.id}}">{{unidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Usuario</label>
		<select ng-model="reporte_expediente.id_usuario">
			<option ng-repeat="usuario in usuarios"
					ng-if="me.id==usuario.id" value="{{usuario.id}}">
				{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Desde	</label>
		<input type="date" ng-model="reporte_expediente.inicio" />
	</div>
	<div class="field-block">
		<label>Hasta</label>
		<input type="date" ng-model="reporte_expediente.fin" />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
	<div class="keypad">
		<button class="ok" ng-click="generateReportExpedientes()">
			<span class="icon-format_list_numbered"></span>
			Generar
		</button>
	</div>
</form>