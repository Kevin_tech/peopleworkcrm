<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<!-- DATOS GENERALES -->
	<h2>Datos Generales</h2>
	<div class="field-block">
		<label>Nombres</label>
		<input type="text" ng-model="usuario.nombre_1" placeholder="Primer nombre" ng-change="generateUsername()" required />
		<input type="text" ng-model="usuario.nombre_2" placeholder="Segundo nombre" ng-change="generateUsername()"/>
		<input type="text" ng-model="usuario.nombre_3" placeholder="Tercer nombre" />
	</div>
	<div class="field-block">
		<label>Apellidos</label>
		<input type="text" ng-model="usuario.apellido_1" placeholder="Primer apellido" ng-change="generateUsername()" required/>
		<input type="text" ng-model="usuario.apellido_2" placeholder="Segundo apellido" ng-change="generateUsername()" />
		<input type="text" ng-model="usuario.apellido_3" placeholder="Apellido de casada" />
	</div>
	<div class="field-block">
		<label>Usuario</label>
		<input type="text" ng-model="usuario.usuario" required />
	</div>
	<div class="field-block">
		<label>País de nacimiento</label>
		<select ng-model="usuario.id_pais_nacimiento">
			<option ng-repeat="pais in paises" value="{{pais.id}}">{{pais.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Departamento de nacimiento</label>
		<select ng-model="usuario.id_departamento_nacimiento">
			<option ng-repeat="departamento in departamentos | filter : { id_pais: usuario.id_pais_nacimiento } : true" value="{{departamento.id}}">
				{{departamento.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipio de nacimiento</label>
		<select ng-model="usuario.id_municipio">
			<option ng-repeat="municipio in municipios | filter : { id_departamento: usuario.id_departamento_nacimiento } : true" value="{{municipio.id}}">
				{{municipio.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Fecha de Nacimiento</label>
		<input type="date" ng-model="usuario.fecha_nacimiento" required />
	</div>
	<div class="field-block">
		<label>Sexo</label>
		<select ng-model="usuario.sexo" required>
			<option value="1">Masculino</option>
			<option value="2">Femenino</option>
		</select>
	</div>
	<div class="field-block">
		<label>Estado Civil</label>
		<select ng-model="usuario.id_estado_civil" required>
			<option ng-repeat="item in estadocivil" value="{{item.id}}">{{item.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Dirección de Residencia Actual</label>
		<input type="text" ng-model="usuario.residencia_direccion" required />
	</div>
	<div class="field-block">
		<label>Departamento de residencia</label>
		<select ng-model="usuario.id_departamento_residencia">
			<option ng-repeat="departamento in departamentos | filter : { id_pais: 1 } : true" value="{{departamento.id}}">
				{{departamento.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipio de residencia</label>
		<select ng-model="usuario.id_municipio_residencia">
			<option ng-repeat="municipio in municipios | filter : { id_departamento: usuario.id_departamento_residencia } : true" value="{{municipio.id}}">
				{{municipio.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Teléfono de Residencia</label>
		<input type="number" ng-model="usuario.residencia_telefono" />
	</div>
	<div class="field-block">
		<label>E-mail personal</label>
		<input type="email" ng-model="usuario.email_personal" />
	</div>
	<div class="field-block">
		<label>NIT</label>
		<input type="text" ng-model="usuario.nit" />
	</div>

	<div class="field-block">
		<label>D.P.I.</label>
		<input type="text" ng-model="usuario.numero_dpi" required />
	</div>
	<div class="field-block">
		<label>No. de Afiliación al IGSS</label>
		<input type="text" ng-model="usuario.numero_igss" />
	</div>
	<div class="field-block">
		<label>No. de Probidad</label>
		<input type="text" ng-model="usuario.numero_probidad" />
	</div>
	<div class="field-block">
		<label>E-mail laboral</label>
		<input type="text" ng-model="usuario.email_laboral" />
	</div>
	<div class="field-block">
		<label>No. de Licencia de Conducir #1</label>
		<input type="text" ng-model="usuario.numero_licencia_1" />
	</div>
	<div class="field-block">
		<label>No. de Licencia de Conducir #2</label>
		<input type="text" ng-model="usuario.numero_licencia_2" />
	</div>
	<!-- ##### -->

	<!-- DATOS FAMILIARES -->
	<h2>Datos Familiares</h2>
	<div class="field-block">
		<label>Nombre del Cónyugue</label>
		<input type="text" ng-model="usuario.conyugue_nombre" />
	</div>
	<div class="field-block">
		<label>Fecha de Nacimiento</label>
		<input type="date" ng-model="usuario.conyugue_fecha_nacimiento" />
	</div>
	<div class="field-block">
		<label>D.P.I.</label>
		<input type="text" ng-model="usuario.conyugue_dpi" />
	</div>
	<div class="field-block">
		<label>País de nacimiento</label>
		<select ng-model="usuario.id_pais_nacimiento_conyugue">
			<option ng-repeat="pais in paises" value="{{pais.id}}">{{pais.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Departamento de nacimiento</label>
		<select ng-model="usuario.id_departamento_nacimiento_conyugue">
			<option ng-repeat="departamento in departamentos | filter : { id_pais: usuario.id_pais_nacimiento_conyugue } : true" value="{{departamento.id}}">
				{{departamento.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipio de nacimiento</label>
		<select ng-model="usuario.id_municipio_conyugue">
			<option ng-repeat="municipio in municipios | filter : { id_departamento: usuario.id_departamento_nacimiento_conyugue } : true" value="{{municipio.id}}">
				{{municipio.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Dirección de Residencia Actual</label>
		<input type="text" ng-model="usuario.conyugue_direccion" />
	</div>
	<div class="field-block">
		<label>Departamento de residencia</label>
		<select ng-model="usuario.id_departamento_residencia_conyugue">
			<option ng-repeat="departamento in departamentos | filter : { id_pais: 1 } : true" value="{{departamento.id}}">
				{{departamento.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipio de residencia</label>
		<select ng-model="usuario.id_municipio_conyugue_residencia">
			<option ng-repeat="municipio in municipios | filter : { id_departamento: usuario.id_departamento_residencia_conyugue } : true" value="{{municipio.id}}">
				{{municipio.nombre}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Teléfono Residencia</label>
		<input type="number" ng-model="usuario.conyugue_telefono" />
	</div>
	<div class="field-block">
		<label>Teléfono Celular</label>
		<input type="number" ng-model="usuario.conyugue_celular" />
	</div>
	<div class="field-block">
		<label>NIT</label>
		<input type="text" ng-model="usuario.conyugue_nit" />
	</div>

	<h3>De los hijos menores o dependientes</h3>
	<div class="field-block">
		<label>Nombre</label>
		<input type="text" ng-model="hijo.nombre" />
	</div>
	<div class="field-block">
		<label>Fecha de Nacimiento</label>
		<input type="date" ng-model="hijo.fecha_nacimiento" />
	</div>
	<div class="field-block">
		<label>Sexo</label>
		<select ng-model="hijo.sexo">
			<option value="1">Hombre</option>
			<option value="0">Mujer</option>
		</select>
	</div>
	<span ng-click="addHijo()" class="btn">Agregar hijo</span>
	<ul class="list-detail">
		<li ng-repeat="item in usuario.hijos">
			<label class="date">{{item.fecha_nacimiento | amDateFormat:'D/MM/YYYY'}}</label>
			<label class="title">{{item.nombre}}</label>
		</li>
	</ul>

	<h3>De los padres</h3>
	<div class="field-block">
		<label>Relación</label>
		<select ng-model="padre.sexo">
			<option value="1">Padre</option>
			<option value="0">Madre</option>
		</select>
	</div>
	<div class="field-block">
		<label>Nombre</label>
		<input type="text" ng-model="padre.nombre" />
	</div>
	<div class="field-block">
		<label>Estado Civil</label>
		<select ng-model="padre.id_estado_civil" required>
			<option ng-repeat="item in estadocivil" value="{{item.id}}">{{item.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Dirección</label>
		<input type="text" ng-model="padre.direccion" />
	</div>
	<div class="field-block">
		<label>Teléfono</label>
		<input type="text" ng-model="padre.telefono" />
	</div>
	<span ng-click="addPadre()" class="btn">Agregar padre</span>
	<ul class="list-detail">
		<li ng-repeat="item in usuario.padres">
			<label class="suptitle" ng-if="item.sexo == 1">Datos del padre</label>
			<label class="suptitle" ng-if="item.sexo == 0">Datos de la madre</label>
			<label class="title">{{item.nombre}}</label>
			<label class="subtitle">{{item.direccion}}</label>
			<label class="subtitle">{{item.telefono}}</label>
		</li>
	</ul>
	<!-- ##### -->

	<!-- INFORMACIÓN ACADEMICA -->
	<h2>Información Académica</h2>
	<div class="field-block">
		<label>Nivel</label>
		<select ng-model="informacion_academica.id_nivel_academico">
			<option ng-repeat="item in nivelacademico" value="{{item.id}}">{{item.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Establecimiento</label>
		<input type="text" ng-model="informacion_academica.establecimiento" />
	</div>
	<div class="field-block">
		<label>Fecha</label>
		<input type="date" ng-model="informacion_academica.fecha" />
	</div>
	<div class="field-block">
		<label>Titulo</label>
		<input type="text" ng-model="informacion_academica.titulo" />
	</div>
	<span ng-click="addInformacionAcademica()" class="btn">Agregar información académica</span>
	<ul class="list-detail">
		<li ng-repeat="item in usuario.informacion_academica">
			<label class="date">{{item.fecha | amDateFormat:'D/MM/YYYY'}}</label>
			<label class="title">{{item.titulo}}</label>
			<label class="subtitle">{{item.establecimiento}}</label>
		</li>
	</ul>
	<!-- ##### -->

	<!-- DATOS EN CASO DE EMERGENCIA -->
	<h2>En caso de emergencia o fallecimiento avisar a</h2>
	<div class="field-block">
		<label>Nombre</label>
		<input type="text" ng-model="emergencia.nombre" />
	</div>
	<div class="field-block">
		<label>Dirección</label>
		<input type="text" ng-model="emergencia.direccion" />
	</div>
	<div class="field-block">
		<label>Teléfono</label>
		<input type="text" ng-model="emergencia.telefono" />
	</div>
	<div class="field-block">
		<label>Parentesco</label>
		<input type="text" ng-model="emergencia.parentesco" />
	</div>
	<span ng-click="addInformacionEmergencia()" class="btn">Agregar contacto de emergencia</span>
	<ul class="list-detail">
		<li ng-repeat="item in usuario.informacion_emergencia">
			<label class="suptitle">{{item.parentesco}}</label>
			<label class="title">{{item.nombre}}</label>
			<label class="subtitle">{{item.direccion}}</label>
			<label class="subtitle">{{item.telefono}}</label>
		</li>
	</ul>
	<!-- ##### -->
	
	<input type="submit" value="Enviar" name="enviar" />
</form>