<h2>Ingresa los detalles de la boleta de autorización de contratos</h2>
<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<h2>Datos Generales</h2>
	<div class="field-block">
		<label>Número de DPI</label>
		<input type="text" ng-model="new_boletaautorizacion.numero_dpi" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Nombres</label>
		<input type="text" ng-model="new_boletaautorizacion.nombre_1" placeholder="Primer nombre" required />
		<input type="text" ng-model="new_boletaautorizacion.nombre_2" placeholder="Segundo nombre"/>
		<input type="text" ng-model="new_boletaautorizacion.nombre_3" placeholder="Tercer nombre" />
	</div>
	<div class="field-block">
		<label>Apellidos</label>
		<input type="text" ng-model="new_boletaautorizacion.apellido_1" placeholder="Primer apellido" required/>
		<input type="text" ng-model="new_boletaautorizacion.apellido_2" placeholder="Segundo apellido" />
		<input type="text" ng-model="new_boletaautorizacion.apellido_3" placeholder="Apellido de casada" />
	</div>
	<div class="field-block">
		<label>Tipo de Servicios <span class="icon-refresh" ng-click="actualizarTiposServicio()"></span></label>
		<select ng-model="new_boletaautorizacion.id_tipo_servicio" required>
			<option ng-repeat="tipo in tiposservicios" value="{{tipo.id}}">{{tipo.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Renglon <span class="icon-refresh" ng-click="actualizarRenglones()"></span></label>
		<select ng-model="new_boletaautorizacion.id_renglon" required>
			<option ng-repeat="renglon in renglones" value="{{renglon.id}}">{{renglon.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Fuente de Financiamiento <span class="icon-refresh" ng-click="actualizarFuentesFinanciamiento()"></span></label>
		<select ng-model="new_boletaautorizacion.id_fuente_financiamiento" required>
			<option ng-repeat="fuentes in fuentesfinanciamiento" value="{{fuentes.id}}">{{fuentes.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Partida Presupuestaria <span class="icon-refresh" ng-click="actualizarPartidasPresupuestarias()"></span></label>
		<select ng-model="new_boletaautorizacion.id_partida_presupuestaria" required>
			<option ng-repeat="partida in partidaspresupuestarias" value="{{partida.id}}">{{partida.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Unidad <span class="icon-refresh" ng-click="actualizarUnidades()"></span></label>
		<select ng-model="new_boletaautorizacion.id_unidad" required>
			<option ng-repeat="unidad in unidades" value="{{unidad.id}}">{{unidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Puesto que ocupa</label>
		<input type="text" ng-model="new_boletaautorizacion.puesto" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Fecha de Inicio</label>
		<input type="date" ng-model="new_boletaautorizacion.fecha_inicio" required />
	</div>
	<div class="field-block">
		<label>Fecha de Finalización</label>
		<input type="date" ng-model="new_boletaautorizacion.fecha_fin" required />
	</div>

	<h2>Datos sobre el pago</h2>
	<div class="field-block">
		<label>Pago inicial</label>
		<input type="number" ng-model="new_boletaautorizacion.pago_1" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Cantidad de pagos</label>
		<input type="number" ng-model="new_boletaautorizacion.pago_cantidad" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Monto de los pagos</label>
		<input type="number" ng-model="new_boletaautorizacion.pago_valor" placeholder="" required />
	</div>

	<input type="submit" value="Enviar" name="enviar" />
</form>