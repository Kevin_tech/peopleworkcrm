<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<label class="seq">
		Correlativo: {{new_documento.correlativo}}
	</label>
	<div class="field-block">
		<label>Asunto</label>
		<!--
		<select 
			ng-model="new_documento.asunto" 
			ng-options="asunto.nombre for asunto in asuntos"
			ng-change="new_documento.id_asunto=new_documento.asunto.id;new_documento.dias_respuesta=new_documento.asunto.dias_respuesta"></select>
		-->
		<input type="text" ng-model="new_documento.asunto" maxlength="150" required />
	</div>
	<!--
	<div class="field-block">
		<label>Institución</label>
		<select ng-model="new_documento.id_institucion">
			<option ng-repeat="institucion in instituciones" value="{{institucion.id}}">{{institucion.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Municipalidad</label>
		<select ng-model="new_documento.id_municipalidad">
			<option ng-repeat="municipalidad in municipalidades" value="{{municipalidad.id}}">{{municipalidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Usuario VoBo</label>
		<select ng-model="new_documento.id_usuario_vobo">
			<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}</option>
		</select>
	</div>
	-->
	<div class="field-block">
		<label># Documento de Referencia</label>
		<input type="text" ng-model="new_documento.documento_referencia" />
	</div>
	<div class="field-block">
		<label>Fecha del Documento de Referencia</label>
		<input type="date" ng-model="new_documento.mi_fecha_referencia" />
	</div>
	<div class="field-block">
		<label>Contenido</label>
		<textarea ng-model="new_documento.contenido" rows="8"></textarea>
	</div>
	<div class="field-block">
		<label>Días para responder</label>
		<input type="number" min="0" string-to-number ng-model="new_documento.dias_respuesta" required />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
</form>