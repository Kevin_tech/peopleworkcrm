<form class="content-block form-block" ng-submit="generateReportOperaciones()">
	<div class="field-block">
		<label>Unidad</label>
		<select ng-model="reporte_operaciones.id_unidad">
			<option ng-repeat="unidad in unidades"
					ng-if="me.id_unidad==unidad.id" value="{{unidad.id}}">{{unidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Usuario</label>
		<select ng-model="reporte_operaciones.id_usuario">
			<option ng-repeat="usuario in usuarios"
					ng-if="me.id==usuario.id" value="{{usuario.id}}">
				{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Desde	</label>
		<input type="date" ng-model="reporte_operaciones.inicio" />
	</div>
	<div class="field-block">
		<label>Hasta</label>
		<input type="date" ng-model="reporte_operaciones.fin" />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
	<div class="keypad">
		<button class="ok" ng-click="generateReportOperaciones()">
			<span class="icon-equalizer"></span>
			Generar
		</button>
	</div>
</form>