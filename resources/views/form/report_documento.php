<form class="content-block form-block" ng-submit="generateReportDocumento()">
	<div class="field-block">
		<label>Correlativo del Documento</label>
		<input type="text" ng-model="reporte_documento.correlativo" required="" />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
	<div class="keypad">
		<button class="ok" ng-click="generateReportDocumento()">
			<span class="icon-library_books"></span>
			Generar
		</button>
	</div>
</form>