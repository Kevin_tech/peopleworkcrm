<h2>Ingresa los datos para el nueva unidad</h2>
<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<div class="field-block">
		<label>Nombre</label>
		<input type="text" ng-model="unidad.nombre" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Siglas</label>
		<input type="text" ng-model="unidad.siglas" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Director de la unidad</label>
		<select ng-model="unidad.id_director">
			<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">
				{{usuario.apellido_1}} {{usuario.apellido_2}},
				{{usuario.nombre_1}} {{usuario.nombre_2}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Gerente</label>
		<select ng-model="unidad.id_gerente">
			<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">
				{{usuario.apellido_1}} {{usuario.apellido_2}},
				{{usuario.nombre_1}} {{usuario.nombre_2}}
			</option>
		</select>
	</div>
	<div class="field-block">
		<label>Supervisor</label>
		<select ng-model="unidad.id_supervisor">
			<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">
				{{usuario.apellido_1}} {{usuario.apellido_2}},
				{{usuario.nombre_1}} {{usuario.nombre_2}}
			</option>
		</select>
	</div>
	<input type="submit" value="Enviar" name="enviar" />
</form>