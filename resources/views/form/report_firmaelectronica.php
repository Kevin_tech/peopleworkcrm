<form class="content-block form-block" ng-submit="generateReportDocumento()">
	<div class="field-block">
		<label>Firma Electrónica</label>
		<input type="text" ng-model="reporte_firmae.signature" required="" />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
	<div class="keypad">
		<button class="ok" ng-click="generateReportFirmaElectronica()">
			<span class="icon-lock"></span>
			Generar
		</button>
	</div>
</form>