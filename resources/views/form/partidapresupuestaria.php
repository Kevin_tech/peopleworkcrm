<h2>Ingresa los detalles de la partida presupuestaria</h2>
<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<h2>Datos Generales</h2>
	<div class="field-block">
		<label>Renglon <span class="icon-refresh" ng-click="actualizarRenglones()"></span></label>
		<select ng-model="new_partidapresupuestaria.id_renglon" required>
			<option ng-repeat="renglon in renglones" value="{{renglon.id}}">{{renglon.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Fuente de Financiamiento <span class="icon-refresh" ng-click="actualizarFuentesFinanciamiento()"></span></label>
		<select ng-model="new_partidapresupuestaria.id_fuente_financiamiento" required>
			<option ng-repeat="fuentes in fuentesfinanciamiento" value="{{fuentes.id}}">{{fuentes.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Actividad</label>
		<input type="text" ng-model="new_partidapresupuestaria.actividad" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Ejercicio Fiscal</label>
		<input type="text" ng-model="new_partidapresupuestaria.ejercicio_fiscal" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Entidad</label>
		<input type="text" ng-model="new_partidapresupuestaria.entidad" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Obra</label>
		<input type="text" ng-model="new_partidapresupuestaria.obra" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Programa</label>
		<input type="text" ng-model="new_partidapresupuestaria.programa" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Proyecto</label>
		<input type="text" ng-model="new_partidapresupuestaria.proyecto" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Sub-Programa</label>
		<input type="text" ng-model="new_partidapresupuestaria.sub_programa" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Ubicación</label>
		<input type="text" ng-model="new_partidapresupuestaria.ubicacion" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Unidad Desconcentrada</label>
		<input type="text" ng-model="new_partidapresupuestaria.unidad_desconcentrada" placeholder="" required />
	</div>
	<div class="field-block">
		<label>Unidad Ejecutora</label>
		<input type="text" ng-model="new_partidapresupuestaria.unidad_ejecutora" placeholder="" required />
	</div>

	<input type="submit" value="Enviar" name="enviar" />
</form>