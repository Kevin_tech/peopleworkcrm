<form class="content-block form-block" enctype="multipart/form-data" ng-submit="submit()">
	<div class="field-block">
		<label>Unidad</label>
		<select ng-model="usuario.id_unidad">
			<option ng-repeat="unidad in unidades" value="{{unidad.id}}">{{unidad.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Profesión</label>
		<select ng-model="usuario.id_profesion">
			<option ng-repeat="profesion in profesiones" value="{{profesion.id}}">{{profesion.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>Puesto</label>
		<select ng-model="usuario.id_puesto">
			<option ng-repeat="puesto in puestos" value="{{puesto.id}}">{{puesto.nombre}}</option>
		</select>
	</div>
	<div class="field-block">
		<label>¿Usuario administrador?</label>
		<label>
			<input type="checkbox" ng-model="usuario.admin" value="1" ng-true-value="'1'" ng-false-value="'0'" />
			Si
		</label>
	</div>
	<div class="field-block">
		<label>Nombres</label>
		<input type="text" ng-model="usuario.nombre_1" placeholder="Primer nombre" ng-change="generateUsername()" required />
		<input type="text" ng-model="usuario.nombre_2" placeholder="Segundo nombre" />
	</div>
	<div class="field-block">
		<label>Apellidos</label>
		<input type="text" ng-model="usuario.apellido_1" placeholder="Primer apellido" ng-change="generateUsername()" required />
		<input type="text" ng-model="usuario.apellido_2" placeholder="Segundo apellido" ng-change="generateUsername()" />
	</div>
	<div class="field-block">
		<label>Usuario</label>
		<input type="text" ng-model="usuario.usuario" required />
	</div>
	<div class="field-block">
		<label>Contraseña</label>
		<input type="text" ng-model="usuario.password" required />
	</div>
	<input type="submit" value="Enviar" name="enviar" />
</form>