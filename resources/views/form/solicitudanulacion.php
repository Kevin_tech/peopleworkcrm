<h2>Solicitud de anulación</h2>
<form class="content-block form-block" ng-submit="submitSolicitudAnulacion()">
	<div class="field-block">
		<label>Indícanos la razón por la cuál deseas anular este documento</label>
		<textarea ng-model="new_anulacion.mensaje" rows="3" required=""></textarea>
	</div>
	<div class="keypad">
		<button class="submit">
			Enviar
		</button>
	</div>
	<input type="submit" value="Enviar" name="enviar" />
</form>