<!DOCTYPE html>
<html ng-app="appTracking">
	<head>
		<title>Bienvenido a Tracking :: INFOM</title>
		<meta charset="utf-8" />
		<meta name="author" content="Kevin Herrarte - (at)kevin_tech" />
		<meta name="description" content="System developed for INFOM Guatemala">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link href="https://fonts.googleapis.com/css?family=Quicksand:400,700,300" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="css/login.css">
		<link rel="stylesheet" type="text/css" href="css/material.css">
		<link rel="stylesheet" type="text/css" href="./bower_components/sweetalert2/dist/sweetalert2.css">
		<link rel="stylesheet" type="text/css" href="./bower_components/angular-loading-bar/build/loading-bar.min.css">
		<script type="text/javascript" src="./bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="./bower_components/angular/angular.min.js"></script>
		<script type="text/javascript" src="./bower_components/angular-resource/angular-resource.min.js"></script>
		<script type="text/javascript" src="./bower_components/ngstorage/ngStorage.min.js"></script>
		<script type="text/javascript" src="./bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
		<script type="text/javascript" src="./bower_components/ngSweetAlert/SweetAlert.min.js"></script>
		<script type="text/javascript" src="./js/login.js"></script>
	</head>

	<body ng-controller="LoginController">
		
		<section id="wrapper">
			<div class="wrap">
				<header>
					<a href="#"><span class="icon-bookmark"></span> + Marcadores</a>
					<a href="manual_tracking.pdf"><span class="icon-live_help"></span> Ayuda</a>
					<a href="http://www.infom.gob.gt" class="button" target="_blank">INFOM.gob.gt</a>
				</header>

				<div class="headline">
					<span class="icon icon-insert_drive_file"></span>
					<span class="message">Bienvenido al sistema de Tracking para documentos oficiales del INFOM</span>
				</div>
			</div>

			<footer>
				Instituto de Fomento Municipal &copy; {{date("Y")}} Todos los derechos reservados. IT INFOM.
			</footer>
		</section>

		<section id="login">
			<div class="wrap">
				<form method="post" ng-submit="submit()">
					<div class="welcome">
						<span class="icon icon-lock_outline"></span>
						<span class="message">Ingresa tus credenciales para iniciar sesión</span>
					</div>

					<div class="line">
						<input type="text" class="textbox" placeholder="Nombre de usuario" ng-model="credencial.usuario" />
					</div>

					<div class="line">
						<input type="password" class="textbox" placeholder="Contraseña" ng-model="credencial.password" />
					</div>

					<div class="line">
						<input id="recordarme" name="recordarme" class="checkbox" type="checkbox" value="1" ng-model="credencial.remember" />
						<label for="recordarme">Recordar sesión</label>
					</div>

					<div class="buttons">
						<button type="submit" class="ok">Iniciar sesión</button>
						<a href="#" class="link">Recuperar mi contraseña</a>
					</div>
				</form>
			</div>
		</section>

	</body>
</html>
