<section class="content-block">
	<header>
		<h2>Pagos Realizados</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="pagoFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'pagos/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo pago con cheque
			</button>
		</div>
		<div class="item" ng-repeat="pago in pagos | filter:pagoFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage | orderBy:created_at:true" 
			ng-click="'pagos/'+pago.id | go">
			<label class="subject">
				DPI: {{pago.numero_dpi}}
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Nombres y apellidos:</strong>
				{{pago.nombre_1}} {{pago.nombre_2}} {{pago.nombre_3}}
				{{pago.apellido_1}} {{pago.apellido_2}} {{pago.apellido_3}}
				<br/>
				<span class="icon-work"></span>
				<strong>Puesto:</strong>
				{{pago.puesto}}
			</div>
			<div>
				<span class="icon-edit"></span>
				<strong>Creado por:</strong>
				{{pago.usuario.apellido_1}} {{pago.usuario.apellido_2}}, {{pago.usuario.nombre_1}} {{pago.usuario.nombre_2}}
				<br/>
				<span class="icon-today"></span>
				{{pago.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="pagination" ng-if="pagos.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="pagos.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>