<section class="content-block">
	<header>
		<h2>Unidades del sistema</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" class="min" ng-model="unidadFiltro" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'unidades/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo unidad
			</button>
		</div>
		<div class="item" ng-repeat="unidad in unidades | filter:unidadFiltro" ng-click="'unidades/'+unidad.id | go">
			<label class="subject">
				{{unidad.nombre}}
			</label>
		</div>
		<div class="item not-found" ng-if="unidades.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>