<section class="content-block">
	<header>
		<h2>Boletas de Autorización de Contratos</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="boletaFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'recursoshumanos/boletasautorizacion/new' | go">
				<span class="icon-add_circle"></span>
				Agregar nueva boleta
			</button>
		</div>
		<div class="item" ng-repeat="boleta in boletasautorizacion | filter:boletaFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage | orderBy:created_at:true" 
			ng-click="'recursoshumanos/boletasautorizacion/'+boleta.id | go">
			<label class="subject">
				DPI: {{boleta.numero_dpi}}
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Nombres y apellidos:</strong>
				{{boleta.nombre_1}} {{boleta.nombre_2}} {{boleta.nombre_3}}
				{{boleta.apellido_1}} {{boleta.apellido_2}} {{boleta.apellido_3}}
				<br/>
				<span class="icon-work"></span>
				<strong>Puesto:</strong>
				{{boleta.puesto}}
			</div>
			<div>
				<span class="icon-edit"></span>
				<strong>Creado por:</strong>
				{{boleta.usuario.apellido_1}} {{boleta.usuario.apellido_2}}, {{boleta.usuario.nombre_1}} {{boleta.usuario.nombre_2}}
				<br/>
				<span class="icon-today"></span>
				{{boleta.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="pagination" ng-if="boletasautorizacion.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="boletasautorizacion.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>