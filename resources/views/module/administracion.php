<div class="col_2_4" ng-if="listview">
	<div ng-include="'list/unidad'"></div>
</div>

<div class="col_2_4" ng-if="listview">
	<div ng-include="'list/puesto'"></div>
</div>

<div class="col_1_3" ng-if="newform || editform" >
	<section class="keypad">
		<button class="cancel" ng-click="'administracion' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Guardar
		</button>
	</section>
</div>

<div class="col_2_3" ng-if="newform" >
	<div ng-if="currentSubModule=='/unidades/new'" ng-include="'form/unidad'"></div>
	<div ng-if="currentSubModule=='/puestos/new'" ng-include="'form/puesto'"></div>
</div>

<div class="col_2_3" ng-if="editform" >
	<div ng-if="regexUnidades.test(currentSubModule)" ng-include="'form/unidad'"></div>
	<div ng-if="regexPuestos.test(currentSubModule)" ng-include="'form/puesto'"></div>
</div>