<div class="col_1_3" ng-if="listview" >
	<h2>Usuarios</h2>
</div>

<div class="col_1_3" ng-if="newform" >
	<section class="keypad">
		<button class="cancel" ng-click="'usuarios' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Guardar
		</button>
	</section>
</div>

<div class="col_2_3">
	<div ng-if="listview" ng-include="'list/usuario'"></div>
	<div ng-if="newform" ng-include="'form/usuario'"></div>
</div>