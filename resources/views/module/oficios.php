<div class="col_1_3" ng-if="listview">
	<h2>Oficios</h2>
</div>

<div class="col_1_3" ng-if="newform" >
	<section class="chose-user">
		<label class="title">
			<span class="icon-person_add"></span>
			Asignar a usuario
		</label>
		<div class="filter">
			<label>Buscar</label>
			<input type="text" ng-model="usuarioFiltro" />
		</div>
		<div class="item" ng-repeat="usuario in usuarios | filter:usuarioFiltro" 
				ng-class="{ selected: usuario.id == oficio.id_usuario_asignado }"
				ng-click="oficio.id_usuario_asignado = (oficio.id_usuario_asignado==usuario.id? 0 : usuario.id)">
			<label class="user">{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}</label>
			<label class="job">{{usuario.puesto.nombre}} | {{usuario.unidad.nombre}}</label>
		</div>
	</section>

	<section class="file-upload">
		<label class="title">
			<span class="icon-add_a_photo"></span>
			Imagen del oficio
		</label>
		<input type="file" file-model="oficio.foto" name="foto" required />
		<span class="detail" ng-if="oficio.foto">Tamaño: {{(oficio.foto.size/1024) | number: 2}}KB</span>
		<img class="preview" ng-src="{{oficio.foto.preview}}" />
	</section>

	<section class="keypad">
		<button class="cancel" ng-click="'documentos' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Guardar
		</button>
	</section>
</div>

<div class="col_2_3">
	<div ng-if="listview" ng-include="'list/oficio'"></div>
	<div ng-if="newform" ng-include="'form/oficio'"></div>
</div>