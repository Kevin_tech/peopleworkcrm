<div class="col_1_4" ng-if="newform" >
	<section class="keypad">
		<button class="cancel" ng-click="'recursoshumanos' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Guardar
		</button>
	</section>
</div>

<div class="col_1_4" ng-if="listview" ng-include="'module/menurecursoshumanos'" ></div>

<div class="col_3_4" ng-if="listview">
	<div ng-include="'list/fuentefinanciamiento'"></div>
</div>

<div class="col_3_4" ng-if="newform">
	<div ng-include="'form/fuentefinanciamiento'"></div>
</div>