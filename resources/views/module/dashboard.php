<div class="col_1_3">
	<h2>Documentos Urgentes</h2>

	<section class="listview-blocks">
		<div class="item" ng-repeat="documento in documentos" 
						ng-init="documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' )"
						ng-if="documento.dias_diferencia_respuesta <= 3 && documento.dias_diferencia_respuesta >= 0 && ( documento.id_estado < 3 || ( documento.id_estado < 3 && documento.fecha_recibido == '0000-00-00 00:00:00' ) || ( documento.id_estado < 3 && documento.fecha_recibido != '0000-00-00 00:00:00' && documentoLeido ) )"
						ng-click="'documentos/'+documento.id | go">
			<span class="date">{{documento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
			<span class="subject">
				{{documento.asunto}}
				<span class="received" ng-if="documento.recibido>0 && documento.visto==0"></span>
				<span class="viewed" ng-if="documento.visto>0"></span>
			</span>
			<div>
				<span class="icon-person"></span>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
				<span class="priority-tag inhurry days">
					{{ documento.dias_diferencia_respuesta | abs }}
				</span>
			</div>
		</div>
	</section>
</div>

<div class="col_1_3">
	<h2>Documentos Retrasados</h2>

	<section class="listview-blocks">
		<div class="item" ng-repeat="documento in documentos" 
						ng-init="documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' )"
						ng-if="documento.dias_diferencia_respuesta < 0 && ( documento.id_estado < 3 || ( documento.id_estado < 3 && documento.fecha_recibido == '0000-00-00 00:00:00' ) || ( documento.id_estado < 3 && documento.fecha_recibido != '0000-00-00 00:00:00' && documentoLeido ) )"
						ng-click="'documentos/'+documento.id | go">
			<span class="date">{{documento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
			<label class="subject">
				{{documento.asunto}}
				<span class="received" ng-if="documento.recibido>0 && documento.visto==0"></span>
				<span class="viewed" ng-if="documento.visto>0"></span>
			</label>
			<div>
				<span class="icon-person"></span>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
				<span class="priority-tag outtime days">
					{{ documento.dias_diferencia_respuesta | abs }}
				</span>
			</div>
		</div>
	</section>
</div>

<div class="col_1_3">
	<h2>Documentos On Time</h2>

	<section class="listview-blocks">
		<div class="item" ng-repeat="documento in documentos" 
						ng-init="documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' )"
						ng-if="( documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' ) ) > 3 && ( documento.id_estado < 3 || ( documento.id_estado < 3 && documento.fecha_recibido == '0000-00-00 00:00:00' ) || ( documento.id_estado < 3 && documento.fecha_recibido != '0000-00-00 00:00:00' && documentoLeido ) )"
						ng-click="'documentos/'+documento.id | go">
			<span class="date">{{documento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
			<span class="subject">
				{{documento.asunto}}
				<span class="received" ng-if="documento.recibido>0 && documento.visto==0"></span>
				<span class="viewed" ng-if="documento.visto>0"></span>
			</span>
			<div>
				<span class="icon-person"></span>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
				<span class="priority-tag ontime days">
					{{ documento.dias_diferencia_respuesta | abs }}
				</span>
			</div>
		</div>
	</section>
</div>

<div class="col_1_3" ng-if="$root.me.puesto.director==1">
	<h2>Monitoreo de Unidad</h2>

	<section class="listview-blocks">
		<div class="item" ng-repeat="documento in documentosDirector" 
						ng-init="documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' )"
						ng-if="documento.id_estado < 3"
						ng-click="'documentos/'+documento.id | go">
			<span class="date">{{documento.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}</span>
			<span class="subject">
				{{documento.asunto}}
				<span class="received" ng-if="documento.recibido>0 && documento.visto==0"></span>
				<span class="viewed" ng-if="documento.visto>0"></span>
			</span>
			<div>
				<span class="icon-person"></span>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
				<span class="priority-tag ontime days"
					ng-class="{ ontime: documento.dias_diferencia_respuesta>3, inhurry: documento.dias_diferencia_respuesta>=0, today: documento.dias_diferencia_respuesta==0, outtime: documento.dias_diferencia_respuesta<0 }">
					{{ documento.dias_diferencia_respuesta | abs }}
				</span>
			</div>
		</div>
	</section>
</div>