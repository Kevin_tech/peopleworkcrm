<div class="col_1_3" ng-if="newform" >
	<section class="file-upload">
		<label class="title">
			<span class="icon-attach_file"></span>
			Archivo adjunto
		</label>
		<input type="file" file-model="usuario.foto" name="foto" required />
		<span class="detail" ng-if="usuario.foto">Tamaño: {{(usuario.foto.size/1024) | number: 2}}KB</span>
		<img class="preview" ng-src="{{usuario.foto.preview}}" />
	</section>

	<section class="keypad">
		<button class="cancel" ng-click="'recursoshumanos' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Guardar
		</button>
	</section>
</div>

<div class="col_1_4" ng-if="listview" ng-include="'module/menurecursoshumanos'" ></div>

<div class="col_3_4" ng-if="listview">
	<div ng-include="'list/usuario'"></div>
</div>

<div class="col_3_4" ng-if="newform">
	<div ng-include="'form/recursoshumanos'"></div>
</div>