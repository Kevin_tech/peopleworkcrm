<ul class="menu-list">
	<li class="title">
		MENU DE NAVEGACIÓN
	</li>
	<li ng-class="{active: currentSubModule == 'boletasautorizacion'}">
		<a href="#/recursoshumanos/boletasautorizacion">
			<span class="icon-portrait"></span>
			Boleta de autorización
		</a>
	</li>
	<li ng-class="{active: currentSubModule == 'contratos'}">
		<a href="#/recursoshumanos/contratos">
			<span class="icon-rate_review"></span>
			Contratos
		</a>
	</li>
	<li ng-class="{active: currentSubModule == 'personal'}">
		<a href="#/recursoshumanos">
			<span class="icon-recent_actors"></span>
			Listado de Personal
		</a>
	</li>
	<li ng-class="{active: currentSubModule == 'fuentesfinanciamiento'}">
		<a href="#/recursoshumanos/fuentesfinanciamiento">
			<span class="icon-attach_money"></span>
			Fuentes de Financiamiento
		</a>
	</li>
	<li ng-class="{active: currentSubModule == 'partidaspresupuestarias'}">
		<a href="#/recursoshumanos/partidaspresupuestarias">
			<span class="icon-format_list_numbered"></span>
			Partidas Prespuestarias
		</a>
	</li>
</ul>