<div class="col_1_3" ng-if="listview" >
	<h2>Pagos</h2>
</div>

<div class="col_1_3" ng-if="newform" >
	<section class="keypad">
		<button class="cancel" ng-click="'pagos' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Enviar
		</button>
	</section>
</div>

<div class="col_2_3">
	<div ng-if="listview" ng-include="'list/pagocheque'"></div>
	<div ng-if="newform || editform" ng-include="'form/pagocheque'"></div>
</div>