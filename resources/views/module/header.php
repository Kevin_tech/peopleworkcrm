<div class="wrap">
	<h1 class="logo" ng-click="'home' | go">logo</h1>
	<nav>
		<ul>
			<li ng-repeat="permiso in usuario.modulos" ng-class="{active: currentModule == permiso.modulo.url}">
				<a href="#/{{permiso.modulo.url}}">
					<span class="icon-{{permiso.modulo.icono}}"></span>
					<span class="legend">{{permiso.modulo.nombre}}</span>
				</a>
			</li>
			<li ng-class="{active: currentModule == 'manual'}">
				<a ng-click="openManual()">
					<span class="icon-book"></span>
					<span class="legend">Manual</span>
				</a>
			</li>
		</ul>
	</nav>
	<div class="user">
		<div class="wrap">
			<div class="notification" ng-click="$root.showNotificacionDropdown = !$root.showNotificacionDropdown">
				<span class="badge">
					{{total_notificaciones = ( notificaciones | filter: { visto : 0 } ).length}}
				</span>
				<span class="icon icon-notifications" ng-class="{ active: $root.showNotificacionDropdown }"></span>
			</div>
			<div class="info">
				<span class="hi">Buen día,</span>
				<span class="name" ng-click="$root.showUserDropdown = !$root.showUserDropdown">{{usuario.nombre_1}} {{usuario.apellido_1}}</span>
			</div>
			<span class="avatar icon-account_circle" ng-click="$root.showUserDropdown = !$root.showUserDropdown"></span>
		</div>

		<section class="dropdown" ng-show="$root.showNotificacionDropdown">
			<ul>
				<li ng-repeat="notificacion in notificaciones | limitTo: 10" class="divider" ng-class="{ important: notificacion.visto == 0 }">
					<a ng-if="notificacion.id_documento > 0" 
						ng-click="view(notificacion, 'documentos', notificacion.id_documento)"
						href="">
						<label class="title">
							<span class="icon-format_list_numbered"></span>
							{{notificacion.tipo.nombre}}
						</label>
						<label class="detail">{{notificacion.documento.asunto | limitTo: 80}}...</label>
						<label class="date">
							<span class="icon-today"></span> 
							{{notificacion.documento.created_at | amDateFormat:'D [de] MMMM [a las] hh:mm a'}}
						</label>
					</a>
					<a ng-if="notificacion.id_oficio > 0" 
						ng-click="view(notificacion, 'oficios', notificacion.id_oficio)"
						href="">
						<label class="title">
							<span class="icon-library_books"></span>
							{{notificacion.tipo.nombre}}
						</label>
						<label class="detail">{{notificacion.oficio.asunto.nombre | limitTo: 80}}...</label>
						<label class="date">
							<span class="icon-today"></span> 
							{{notificacion.oficio.created_at | amDateFormat:'D [de] MMMM [a las] hh:mm a'}}
						</label>
					</a>
				</li>
			</ul>
		</section>

		<section class="dropdown" ng-show="$root.showUserDropdown">
			<ul>
				<li>
					<a href="#/usuarios/{{usuario.id}}">
						<span class="icon icon-vpn_key"></span>
						Cambiar contraseña
					</a>
				</li>
				<li>
					<a href="#/logout">
						<span class="icon icon-exit_to_app"></span>
						Salir
					</a>
				</li>
			</ul>
		</section>
	</div>
</div>