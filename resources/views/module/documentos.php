<div class="col_1_3" ng-if="listview" >
	<h2>Documentos</h2>
</div>

<div class="col_1_3" ng-if="newform" >
	<section class="choose-depto">
		<select ng-model="usuarioFiltro.id_unidad">
			<option value="">Elija una unidad...</option>
			<option ng-repeat="unidad in unidades" value="{{unidad.id}}">
				{{unidad.nombre}}
			</option>
		</select>
	</section>
	<section class="choose-user" ng-show="usuarioFiltro.id_unidad>0">
		<label class="title">
			<span class="icon-person_add"></span>
			Enviar a:
		</label>
		<div class="filter">
			<label>Buscar</label>
			<input type="text" ng-model="usuarioFiltro.$" />
		</div>
		<div class="item" ng-repeat="usuario in usuarios | filter:usuarioFiltro" 
				ng-class="{ selected: usuario.id == new_documento.id_usuario_asignado }"
				ng-if="me.id_unidad==usuario.id_unidad || usuario.id_puesto==2"
				ng-click="new_documento.id_usuario_asignado = (new_documento.id_usuario_asignado==usuario.id? 0 : usuario.id)">
			<label class="user">{{usuario.apellido_1}} {{usuario.apellido_2}}, {{usuario.nombre_1}} {{usuario.nombre_2}}</label>
			<label class="job">{{usuario.puesto.nombre}} | {{usuario.unidad.nombre}}</label>
		</div>
	</section>

	<section class="file-upload">
		<label class="title">
			<span class="icon-attach_file"></span>
			Archivo adjunto
		</label>
		<input type="file" file-model="new_documento.foto" name="foto" required />
		<span class="detail" ng-if="new_documento.foto">Tamaño: {{(new_documento.foto.size/1024) | number: 2}}KB</span>
		<!--<img class="preview" ng-src="{{new_documento.foto.preview}}" />-->
	</section>

	<section class="keypad">
		<button class="cancel" ng-click="'documentos' | go">
			Regresar
		</button>
		<button class="submit" ng-click="submit()">
			Enviar
		</button>
	</section>
</div>

<div class="col_2_3">
	<div ng-if="listview" ng-include="'list/documento'"></div>
	<div ng-if="newform" ng-include="'form/documento'"></div>
</div>