<!DOCTYPE html>
<html ng-app="appTracking">
	<head>
		<title ng-bind="title">Inicio Tracking :: INFOM</title>
		<meta charset="utf-8" />
		<meta name="author" content="Kevin Herrarte - (at)kevin_tech" />
		<meta name="description" content="System developed for INFOM Guatemala">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link href="https://fonts.googleapis.com/css?family=Quicksand:400,700,300" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="./css/all.css">
		<script type="text/javascript" src="./js/all.js"></script>
	</head>

	<body>
	
		<header ng-include="'module/header'" ng-controller="HeaderController"></header>
		
		<section class="wrapper" ng-view></section>

	</body>
</html>
