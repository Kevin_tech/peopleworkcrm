<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">

		html,body
		{
			font-family: Arial;
			font-size: 16px;
			padding-top: 180px;
			padding-bottom: 45px;
		}

		table
		{
			border-collapse: collapse;
		}

		.avatar, .cheque
		{
			border: 4px solid black;
			position: absolute;
			right: 10px;
			width: 220px;
		}

		.cheque
		{
			top: 375px;
		}

		.footer
		{
			bottom: 0;
			height: 60px;
			position: fixed;
			right: 0;
			text-align: center;
			width: 100%;
		}

		.footer img
		{
			width: 100%;
		}

		.itinfom
		{
			bottom: 50px;
			font-size: 12px;
			height: 30px;
			position: fixed;
			right: 0;
			text-align: right;
			width: 100px;
		}

		.itinfom img
		{
			height: 100%;
		}

		.leyenda
		{
			padding-top: 35px;
		}

		.list
		{
			font-size: 18px;
			margin: 0;
			padding: 0;
		}

		.list label
		{
			display: block;
			font-size: 18px;
			padding: 3px 0;
			text-align: center;
			width: 400px;
		}

		.list .sub
		{
			display: block;
			font-size: 14px;
			text-align: center;
			width: 400px;
		}

		.logo
		{
			height: 150px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.pagenum:before
		{
			content: counter(page);
		}

		.page-break
		{
			page-break-after: always;
		}


		.signatures
		{
			width: 100%;
			position: relative;
		}

		.signatures > div
		{
			border-top: 1px solid black;
			font-weight: bold;
			text-align: center;
			margin-top: 50px;
			position: absolute;
			width: 220px;
		}

		.signatures .center
		{
			left: 50%;
			margin-left: -150px;
			top: 100px;
			width: 300px;
		}

		.signatures .left
		{
			left: 1em;
		}

		.signatures .right
		{
			right: 1em;
		}

		.titulo
		{
			font-size: 20px;
			font-weight: bold;
			padding-left: 200px;
			position: fixed;
			text-align: center;
			top: 25px;
			width: 100%;
		}
	</style>
</head>
<body>

	<div class="logo">
		
	</div>

	<div class="titulo">
		BOLETA DE AUTORIZACIÓN PARA LA ELABORACIÓN DE CONTRATOS ADMINISTRATIVOS 2016
	</div>

	<div class="footer">
		
	</div>

	<div class="itinfom">
		
		<br/>Página: <span class="pagenum"></span> / 
		<script type="text/php">
			$text = '{PAGE_COUNT}';
			$font = Font_Metrics::get_font("arial");
			$pdf->page_text(580, 721, $text, $font, 8);
		</script>
	</div>

	<p>
		Licenciado <br/>
		Estuardo Valdez Bonilla <br/>
		Director Técnico de Recursos Humanos <br/>
		Instituto de Fomento Municipal -INFOM- <br/>
		Su despacho
	</p>

	<p>
		Licenciado Valdez:
	</p>

	<p>
		Con base a la evaluación circular y análisis de los Términos de Referencia, realizados por esta Subgerencia, aprueba la elaboración del siguiente Contrato:
	</p>

	<p class="list">
		<b>Nombres y apellidos: </b>
			{{ucfirst(strtolower($data->nombre_1))}} {{ucfirst(strtolower($data->nombre_2))}} {{ucfirst(strtolower($data->nombre_3))}}
			{{ucfirst(strtolower($data->apellido_1))}} {{ucfirst(strtolower($data->apellido_2))}} {{ucfirst(strtolower($data->apellido_3))}}
		<br/><br/>
		
		<b>Número de DPI: </b>
			{{$data->numero_dpi}}
		<br/><br/>
		
		<b>Tipo de Servicio: </b>
			{{$data->tipo_servicio->nombre}}
		<br/><br/>

		<b>Duración del contrato: </b>
			Inicio: {{$data->fecha_inicio}}
			Final: {{$data->fecha_fin}}
		<br/><br/>
		
		<b>Forma de Pago: </b>
			Un primer pago de Q. {{$data->pago_1}}
			@if($data->pago_cantidad > 0) 
				y {{$data->pago_cantidad}} de Q. {{$data->pago_valor}}
			@endif
		<br/><br/>

		<b>Monto Total del Contrato: </b>
			@if($data->pago_cantidad > 0) 
				Q. {{( $data->pago_1 + ($data->pago_cantidad * $data->pago_valor) )}}
			@else 
				Q. {{$data->pago_1}}
			@endif
		<br/><br/>

		<b>Partida Presupuestaria: </b>
			{{$data->partida_presupuestaria->actividad}} -
			{{$data->partida_presupuestaria->ejercicio_fiscal}} -
			{{$data->partida_presupuestaria->entidad}} -
			{{$data->partida_presupuestaria->obra}} -
			{{$data->partida_presupuestaria->programa}} -
			{{$data->partida_presupuestaria->proyecto}} -
			{{$data->partida_presupuestaria->sub_programa}} -
			{{$data->partida_presupuestaria->ubicacion}} -
			{{$data->partida_presupuestaria->unidad_desconcentrada}} -
			{{$data->partida_presupuestaria->unidad_ejecutora}}
		<br/><br/>
		
		<b>Puesto que ocupa: </b>
			{{ucfirst(strtolower($data->puesto))}}
		<br/><br/>

		<b>Unidad: </b>
			{{ucfirst(strtolower($data->unidad->nombre))}}
		<br/><br/>
	</p>

	<p class="leyenda">
		<b>Observaciones: </b>
		<br/>
		Atentamente,
	</p>

	<div class="signatures">
		<div class="center">
			Ing. Julio César Melgar Samayoa <br/>
			Subgerente
		</div>
	</div>

</body>
</html>