<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		html,body
		{
			font-family: Arial;
			font-size: 14px;
			padding-top: 70px;
		}

		table
		{
			border-collapse: collapse;
		}

		table td
		{
			padding: 0.25em 0.5em;
		}

		.cabecera
		{
			text-align: center;
			left: 0;
			position: fixed;
			top: 0;
			width: 100%;
		}

		.cabecera h1
		{
			font-size: 16px;
			margin: 0;
		}
		
		.cabecera h2
		{
			font-size: 14px;
			margin: 0;
		}

		.cabecera h3
		{
			font-size: 14px;
			margin: 0;
		}

		.cuerpo
		{
			width: 100%;
		}

		.cuerpo thead
		{
			border: 1px solid black;
		}

		.cuerpo thead th
		{
			padding: 0.5em;
		}

		.cuerpo tbody tr:nth-child(even)
		{
			background: #CEECFA;
		}

		.cuerpo tbody tr:nth-child(odd)
		{
			background: white;
		}

		.cuerpo .total
		{
			background: white;
			font-weight: bold;
			text-align: center;
		}

		.cuerpo .unidad
		{
			background: white;
			font-size: 16px;
			font-weight: bold;
			text-transform: uppercase;
		}

		.cuerpo .unidad td
		{
			padding: 1em;
		}

		.cuerpo .usuario
		{
			background: white;
			font-weight: bold;
		}

		.cuerpo .usuario td
		{
			padding-left: 2em;
		}

		.itinfom
		{
			bottom: 0;
			height: 42px;
			position: fixed;
			right: 0;
			text-align: center;
			width: 100%;
		}

		.itinfom img
		{
			height: 100%;
		}

		.itinfom .digitalsignature
		{
			font-size: 12px;
		}

		.logo
		{
			height: 36px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.page-break
		{
			page-break-after: always;
		}

		.pagenum:before
		{
			content: counter(page);
		}

		.resumen
		{
			border: 1px solid black;
			font-size: 11px;
			margin-bottom: 1em;
			padding: 1em;
			position: absolute;
			right: 0;
			top: 0;
			width: 220px;
		}

		.resumen h1
		{
			background: white;
			font-size: 12px;
			margin: -1.5em auto 0;
			padding: 0;
			text-align: center;
			width: 110px;
		}
	</style>
</head>
<body>

	<div class="logo">
		<img src="{{public_path()}}/img/infom.png" />
	</div>

	<div class="itinfom">
		<img src="{{public_path()}}/img/it.png" />
		<div class="digitalsignature">
			<b>Firma digital:</b>
			{{$digital_signature}}
		</div>
	</div>

	<div class="cabecera">
		<h1>INFOM - Sistema de Tracking</h1>
		<h2>Expedientes Finalizados</h2>
		<h3>Del {{date("d/m/Y", strtotime($inicio))}} Al {{date("d/m/Y", strtotime($inicio))}}</h3>

		<div class="resumen">
			<h1>EXPEDIENTES</h1>
			<label><b>Fecha:</b> {{date("d/m/Y")}}</label>
			<label><b>Hora:</b> {{date("H:i:s")}}</label><br/>
			<label><b>Página:</b> <span class="pagenum"></span></label>
		</div>
	</div>

	<table class="cuerpo">
		<thead>
			<tr>
				<th>#Expediente</th> 
				<th>Enviado Por</th>
				<th> Dirigido A</th>
				<th>Asunto</th>
				<th>No. Documento </br> referencia</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$unidad_actual 	= "";
				$expedientes_usuario	= 0;
				$id_unidad_actual = 0;
				$id_usuario_actual 		= 0;
			?>
			@foreach( $data as $documento )
				@foreach( $documento->asignacion as $asignacion )
					@if( $asignacion->activo )
						@if( $asignacion->usuario_asignado->id != $id_usuario_actual && $id_usuario_actual > 0 )
							<tr class="total">
								<td colspan="5">Total de expedientes del usuario: {{$expedientes_usuario}}</td>
							</tr>
							<?php $expedientes_usuario = 0; ?>
						@endif
						@if( $asignacion->usuario_asignado->id_unidad != $id_unidad_actual )
							<tr class="unidad">
								<td colspan="5">UNIDAD: {{$asignacion->usuario_asignado->unidad->nombre}}</td>
							</tr>
							<?php 
								$unidad_actual 	= $asignacion->usuario_asignado->unidad->nombre;
								$id_unidad_actual = $asignacion->usuario_asignado->id_unidad;
							?>
						@endif
						@if( $asignacion->usuario_asignado->id != $id_usuario_actual )
							<tr class="usuario">
								<td colspan="5">
									{{$asignacion->usuario_asignado->apellido_1}} {{$asignacion->usuario_asignado->apellido_2}},
									{{$asignacion->usuario_asignado->nombre_1}} {{$asignacion->usuario_asignado->nombre_2}}
								</td>
							</tr>
							<?php 
								$id_usuario_actual = $asignacion->usuario_asignado->id;
							?>
						@endif
					@endif
				@endforeach
				<tr>
					<td>{{$documento->correlativo}}</td> 
					<td>
						{{$documento->usuario_creo->apellido_1}} {{$documento->usuario_creo->apellido_2}},
						{{$documento->usuario_creo->nombre_1}} {{$documento->usuario_creo->nombre_2}}
					</td>
					<td>{{$unidad_actual}}</td>
					<td>{{$documento->asunto}}</td>
					<td>{{$documento->documento_referencia}}</td>
				</tr>
				<?php $expedientes_usuario++; ?>
			@endforeach
			<tr class="total">
				<td colspan="5">Total de expedientes del usuario: {{$expedientes_usuario}}</td>
			</tr>
		</tbody>
	</table>

</body>
</html>