<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		html,body
		{
			font-family: Arial;
			font-size: 14px;
			padding-bottom: 10px;
			padding-top: 70px;
		}

		table
		{
			border-collapse: collapse;
		}

		table td
		{
			padding: 0.25em 0.5em;
		}

		.cabecera
		{
			text-align: center;
			left: 0;
			position: fixed;
			top: 0;
			width: 100%;
		}

		.cabecera h1
		{
			font-size: 16px;
			margin: 0;
		}
		
		.cabecera h2
		{
			font-size: 14px;
			margin: 0;
		}

		.cabecera h3
		{
			font-size: 14px;
			margin: 0;
		}

		.cuerpo
		{
			width: 100%;
		}

		.cuerpo thead
		{
			border: 1px solid black;
		}

		.cuerpo thead th
		{
			padding: 0.5em;
		}

		.cuerpo tbody tr:nth-child(even)
		{
			background: #CEECFA;
		}

		.cuerpo tbody tr:nth-child(odd)
		{
			background: white;
		}

		.cuerpo .cantidad
		{
			text-align: right;
		}

		.cuerpo .total
		{
			background: white;
			font-weight: bold;
			text-align: right;
		}

		.cuerpo .total label
		{
			background: #CEECFA;
			border: 1px solid black;
			display: inline-block;
			padding: 0.5em;
			text-align: center;
			max-width: 250px;
		}

		.cuerpo .unidad
		{
			background: white;
			font-size: 16px;
			font-weight: bold;
			text-transform: uppercase;
		}

		.cuerpo .unidad td
		{
			padding: 1em;
		}

		.cuerpo .usuario
		{
			background: white;
			font-weight: bold;
		}

		.cuerpo .usuario td
		{
			padding-left: 2em;
		}

		.itinfom
		{
			bottom: -22px;
			height: 42px;
			position: fixed;
			right: 0;
			text-align: right;
			width: 100px;
		}

		.itinfom img
		{
			height: 100%;
		}

		.logo
		{
			height: 36px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.page-break
		{
			page-break-after: always;
		}

		.pagenum:before
		{
			content: counter(page);
		}

		.pie
		{
			border-top: 2px solid black;
			bottom: 25px;
			font-size: 12px;
			left: 0;
			padding: 1em 0 0 0;
			position: fixed;
			width: 100%;
		}

		.pie .digitalsignature
		{
			bottom: -8px;
			font-size: 12px;
			position: fixed;
			text-align: center;
			width: 100%;
		}

		.pie .referencia
		{
			display: inline-block;
			max-width: 125px;
			vertical-align: top;
		}

		.resumen
		{
			border: 1px solid black;
			font-size: 11px;
			margin-bottom: 1em;
			padding: 1em;
			position: absolute;
			right: 0;
			top: 0;
			width: 220px;
		}

		.resumen h1
		{
			background: white;
			font-size: 12px;
			margin: -1.5em auto 0;
			padding: 0;
			text-align: center;
			width: 110px;
		}

		.subrayado
		{
			border-bottom: 1px solid black;
		}
	</style>
</head>
<body>

	<div class="logo">
		<img src="{{public_path()}}/img/infom.png" />
	</div>

	<div class="cabecera">
		<h1>INFOM - Sistema de Tracking</h1>
		<h2>Registro y Operaciones de Documentos por Usuario</h2>
		<h3>Del {{date("d/m/Y", strtotime($inicio))}} Al {{date("d/m/Y", strtotime($fin))}}</h3>

		<div class="resumen">
			<h1>EXPEDIENTES</h1>
			<label><b>Fecha:</b> {{date("d/m/Y")}}</label>
			<label><b>Hora:</b> {{date("H:i:s")}}</label><br/>
			<label><b>Página:</b> <span class="pagenum"></span></label>
		</div>
	</div>

	<div class="pie">
		<div class="itinfom">
			<img src="{{public_path()}}/img/it.png" />
		</div>
		<div class="referencia">
			<b>Ing</b>
			=
			Ingresados
		</div>
		<div class="referencia">
			<b>Tra</b>
			=
			Trasladados
		</div>
		<div class="referencia">
			<b>No Rec</b>
			=
			No Recibidos
		</div>
		<div class="referencia">
			<b>Rec</b>
			=
			Recibidos
		</div>
		<div class="referencia">
			<b>Pro</b>
			=
			En Proceso
		</div>
		<div class="referencia">
			<b>Anu</b>
			=
			Anulados
		</div>
		<div class="referencia">
			<b>Sus</b>
			=
			Suspendidos
		</div>
		<div class="referencia">
			<b>Rech</b>
			=
			Rechazados
		</div>
		<div class="referencia">
			<b>Fin</b>
			=
			Finalizados
		</div>
		<div class="digitalsignature">
			<b>Firma digital:</b>
			{{$digital_signature}}
		</div>
	</div>

	<table class="cuerpo">
		<thead>
			<tr>
				<th rowspan="2">Nombre de Usuario</th> 
				<th colspan="9" class="subrayado">E X P E D I E N T E S</th>
			</tr>
			<tr>
				<th>Ing</th>
				<th>Tra</th>
				<th>No Rec</th>
				<th>Rec</th>
				<th>Pro</th>
				<th>Anu</th>
				<th>Sus</th>
				<th>Rech</th>
				<th>Fin</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$id_unidad_actual = 0;
				$usuarios_unidad	= 0;
			?>
			@foreach($data as $usuario)
				@if( $usuario->id_unidad != $id_unidad_actual && $id_unidad_actual > 0 )
					<tr class="total">
						<td colspan="10">
							<label>Usuarios por Unidad: {{$usuarios_unidad}}</label>
						</td>
					</tr>
					<?php $usuarios_unidad = 0; ?>
				@endif
				@if( $usuario->id_unidad != $id_unidad_actual )
					<tr class="unidad">
						<td colspan="10">UNIDAD: {{$usuario->unidad->nombre}}</td>
					</tr>
					<?php $id_unidad_actual = $usuario->id_unidad; ?>
				@endif
				<tr>
					<td>
						{{$usuario->apellido_1}} {{$usuario->apellido_2}},
						{{$usuario->nombre_1}} {{$usuario->nombre_2}}
						- [{{$usuario->usuario}}]
					</td>
					<td class="cantidad">{{$usuario->documentoingresado}}</td>
					<td class="cantidad">{{$usuario->documentotrasladado}}</td>
					<td class="cantidad">0</td>
					<td class="cantidad">{{$usuario->documentorecibido}}</td>
					<td class="cantidad">{{$usuario->documentoproceso}}</td>
					<td class="cantidad">0</td>
					<td class="cantidad">0</td>
					<td class="cantidad">0</td>
					<td class="cantidad">{{$usuario->documentofin}}</td>
				</tr>
				<?php $usuarios_unidad++; ?>
			@endforeach
			<tr class="total">
				<td colspan="10">
					<label>Usuarios por Unidad: {{$usuarios_unidad}}</label>
				</td>
			</tr>
		</tbody>
	</table>

</body>
</html>