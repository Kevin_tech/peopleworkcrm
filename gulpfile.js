var concat 		= require("gulp-concat"),
	concatCss 	= require("gulp-concat-css"),
	gulp 		= require("gulp"),
	ngAnnotate 	= require("gulp-ng-annotate"),
	source 		= require("vinyl-source-stream"),
	stylus 		= require("gulp-stylus"),
	uglify 		= require("gulp-uglify"),
	uglifycss 	= require("gulp-uglifycss");

var paths		= {
	allcss:		"./public/css/all.css",
	alljs:		"./public/js/all.js",
	appjs:		"./public/js/app.js",
	appstyl:	"./public/css/app.styl",
	scripts: 	[
					"./public/js/socket.io.min.js",
					"./public/bower_components/sweetalert2/dist/sweetalert2.min.js",
					"./public/bower_components/moment/min/moment.min.js",
					"./public/bower_components/moment/locale/es.js",
			        "./public/bower_components/angular/angular.min.js",
			        "./public/bower_components/angular-camera/dist/angular-camera.min.js",
					"./public/bower_components/angular-resource/angular-resource.min.js",
					"./public/bower_components/angular-route/angular-route.min.js",
					"./public/bower_components/angular-sanitize/angular-sanitize.min.js",
					"./public/bower_components/ngstorage/ngStorage.min.js",
					"./public/bower_components/angular-loading-bar/build/loading-bar.min.js",
					"./public/bower_components/angular-moment/angular-moment.min.js",
					"./public/bower_components/angular-file-model/angular-file-model.js",
					"./public/bower_components/angular-socket-io/socket.min.js",
					"./public/bower_components/ng-dialog/js/ngDialog.min.js",
					"./public/bower_components/ng-simplePagination/simplePagination.js",
					"./public/bower_components/ngSweetAlert/SweetAlert.min.js",
					"./public/js/app.js"
				],
	styles:		[
			        "./public/css/material.min.css",
			        "./public/bower_components/angular-loading-bar/build/loading-bar.min.css",
			        "./public/bower_components/sweetalert2/dist/sweetalert2.css",
			        "./public/bower_components/ng-dialog/css/ngDialog.css",
			        "./public/bower_components/ng-dialog/css/ngDialog-theme-default.css",
			        "./public/css/app.css"
			    ]
};

gulp.task("bundlecss", ["stylus"], function() {
	return gulp.src( paths.styles )
	    .pipe(concatCss("all.css"))
	    .pipe(gulp.dest("./public/css"));
});

gulp.task("bundlejs", ["nguglify"], function() {
	return gulp.src( paths.scripts )
	    .pipe(concat("all.js"))
	    .pipe(gulp.dest("./public/js/"));
});

gulp.task("compresscss", ["bundlecss"], function () {
	return gulp.src( paths.allcss )
		.pipe(uglifycss())
		.pipe(gulp.dest("./public/css/"));
});

gulp.task("compressjs", ["bundlejs"], function() {
	return gulp.src( paths.alljs )
		.pipe(uglify())
		.pipe(gulp.dest("./public/js"));
});

gulp.task("nguglify", function () {
	return gulp.src( paths.appjs )
		.pipe(ngAnnotate())
		.pipe(gulp.dest("./public/js"));
});

gulp.task("stylus", function () {
	return gulp.src( paths.appstyl )
		.pipe(stylus({
				compress: true,
				"include css": true
			}))
		.pipe(gulp.dest("./public/css"));
});

// Rerun the task when a file changes
gulp.task("watch", ["compressjs", "compresscss"], function() {
	gulp.watch( paths.scripts, ["compressjs"] );
	gulp.watch( paths.styles, ["compresscss"] );
	gulp.watch( paths.appstyl, ["stylus"] );
});

gulp.task("default", ["bundlecss", "bundlejs", "compresscss", "compressjs", "nguglify", "stylus", "watch"]);